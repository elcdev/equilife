import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

const PrivacyPage = () => (
  <Layout>
    <SEO title='Privacy' />
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>Privacy Policy</h3>
            <p className='section-main'>When you use our website, there may be times when you provide certain
            private and personal information about yourself to us. This may be especially true if you are a
            policyowner and you contact us wishing to access information concerning your coverage, a pending
            claim for benefits or to take advantage of various customer services that we can provide to you
            on our website.
            From the outset, we want you to know that we do respect your privacy. After all, we are in a
            business of trust, so keeping your personal information confidential and protected against
            unauthorized disclosure is very important to us.
            We intend to limit the amount of personal information needed by us to the minimum amount
            necessary to deliver the service you expect from us. While this will allow us to serve you
            better, at the same time it lets you know that we are not going to seek any more information
            than we will need to get the job done.
            To access our website, you will not need to provide us with any personal information about
            yourself. You will not be required to "log in" with a password or to register to use our
            website. Additionally, unless you contact us and give us certain information about yourself,
            your access and use of our website is anonymous. We only know how many "hits" we get on our
            website, but we do not know who is in fact using the site unless they tell us.
            We do provide a Contact Us feature on our website that does require you to provide to us certain
            personal information about yourself, like your name, address, policy number, telephone number or
            e-mail address. We will need this information to assist in any inquiry you may have and to
            determine the best way to respond to you to assure your needs have been met. Additionally, we
            may use this information to send you periodic information about us, our products and services,
            or topics of interest related to our products or business.
            You may wish to have more specific information about a particular product than what is provided
            to you on our website. In these instances, we may forward your request to a professional
            Equitable agent in your area to help you. We remind you that you are under no obligation to meet
            with an agent. But we have found, through decades of experience, that having a professional
            insurance representative in your own community that you can talk to is often the best way for
            you to get your questions answered and to assist you in your decision making.
            If you meet with a professional Equitable agent you may also find that you may disclose more
            personal information, usually health information, that guides you in determining if you can
            qualify to purchase a certain life or health insurance policy from us. Be assured that our agent
            representatives respect your privacy as well and will protect your personal information from
            unauthorized disclosure with the same degree of diligence that we do.
            Employees or agents who violate our privacy policy are subject to disciplinary action by us. We
            train all employees on HIPAA compliance and the importance of the privacy and confidentiality of
            all information we collect.
            We will maintain all personal information that you give to us in a secured database. You retain
            the right to "remove" this information from our database by simply contacting us and telling us
            to do so.
            We do not and will not share personal information you give to us with any unauthorized third
            party without your written permission. This means we do not sell or market our customer lists to
            anyone.
            There may be instances where we are compelled, by law, to disclose personal information provided
            to us. This would be, for instance, under a valid court order or subpoena, or at the request of
            insurance regulatory officials in their capacity as investigators or examiners. In any such
            instance, we would only disclose the information requested and would seek, through protective
            order or other legal means, the ability to keep this information confidential and not subject to
            disclosure to any other person.
            We are sure you have heard about situations where a "hacker" gains access to a business through
            a website and causes mischief by exposing customer accounts, credit card numbers, or financial
            information about the business. Guarding against being trespassed upon in this way is a matter
            we take seriously. While we cannot guarantee that this will never happen, we do employ various
            security measures that alert us to whether unauthorized access to us has been attempted - kind
            of like a burglar alarm system. We also use "firewalls" designed to accept what and whom we want
            accessing our system and to reject what we feel is unwanted or unauthorized access.
            Some websites attach a "cookie" onto your computer when you access the site. Cookies are
            commonly used to store information, like passwords, and can be generally helpful in your
            repeated use of a website. Cookies can also be used to set up a customer profile, which a
            business may market to another business, and result in your receiving advertisements or
            solicitations, whether you want them or not. As part of our total commitment to protecting your
            privacy, we do not engage in "online profiling" and when you access our website we will not send
                        or attach a "cookie" to your computer at any time.</p>

            <p>If you have questions about our privacy policy or information practices, you can contact us at:
                    </p>
            <p>Equitable Life & Casualty </p>
            <p>Attn: Legal Department </p>
            <p>299 South Main Street, Suite 1100</p>
            <p>Salt Lake City, Utah 84111</p>
            <p>1-800-352-5150 </p>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default PrivacyPage
