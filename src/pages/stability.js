import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import HeaderImage from '../images/Headers_16.png'
import Chart from '../images/q1chart-2020.png'
import BannerCenter from '../images/B+ banner-center.png'
import FinancialStabilityPDF from '../assets/ELCFinStrength-Q1-2020.pdf'

const StabilityPage = () => (
  <Layout>
    <SEO title='Financial Stability' />
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={HeaderImage} alt='Financial Stability' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-12 section-header text-center'>

            <h3 className='section-title underline longer-underline'><i className='fas fa-chart-pie'></i> Financial
                        Stability</h3>
            <div className='image'> <img className='image' src={BannerCenter}
              style={{ maxWidth: '60%', width: 'auto/9', maxHeight: '100%' }} alt='B+ banner' />
            </div>
            <br />

          </div>
          <div className='col-md-6 feature text-center'>
            <section className='section'>
              <p>The bedrock of our financial strength rests on our strong capital position and conservative
              investment portfolio. It gives our policyowners peace of mind, knowing we have the long-term
                            ability to keep our promises, to be there for them when they need us. </p>
              <p className='section-subtitle'>Financial security enables us to meet the changing needs of our
                            customers while engaging in an increasingly competitive marketplace.</p>
              <p>Our investment strategy includes a high quality portfolio focused on investments in
              publicly-traded bonds, structured securities, and commercial mortgage loans. Our portfolio
              continues to be well-diversified, with the majority of assets carrying an Investment Grade
                            (NAIC 1&2) rating. </p>
            </section>
          </div>
          <div className='col-md-6 feature text-center'>
            <div className='img-thumbnail' style={{ maxHeight: '100%', maxWidth: '80%', width: 'auto/9' }}>
              <img className='img-thumbnail' src={Chart} alt='chart' />
            </div>
          </div>

        </div>
      </div>
      <br />
      <div className='col-md-12 feature text-center'>

        <p className='section-subtitle'>To view a copy of our current financial strength sheet, please <a
          href={FinancialStabilityPDF}><u>click here.</u></a></p>
      </div>
    </section>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>product development philosophy</h3>
          </div>
          <div className='col-md-6'>
            <div className='feature-box'>
              <div className='icon'>
                <i className='fa fa-funnel-dollar'></i>
              </div>
              <h5 className='feature-title'><b>1. Competitiveness</b></h5>
              <p className='feature-desc'>Our products are designed to fill a specific need, whether that be
                            guaranteed accumulation, indexed accumulation, etc.</p>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='feature-box'>
              <div className='icon'>
                <i className='fas fa-eye'></i>
              </div>
              <h5 className='feature-title'><b>2. Transparency</b></h5>
              <p className='feature-desc'>For every product feature that is added, we make sure it is transparent,
                            not overly complicated and adds value.</p>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='feature-box'>
              <div className='icon'>
                <i className='fa fa-equals'></i>
              </div>
              <h5 className='feature-title'><b>3. Consistency</b></h5>
              <p className='feature-desc'>In order to maintain consistency in rates over time, we spend the same
                            amount on option budgets each year. That means great rates <i>and</i> great renewals.</p>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='feature-box'>
              <div className='icon'>
                <i className='fa fa-balance-scale'></i>
              </div>
              <h5 className='feature-title'><b>4. A Financial Responsibility</b></h5>
              <p className='feature-desc'>We offer financially responsible products for the carrier, the agent,
              and the policyholder, insuring that all of our interests are aligned. We strive to create
                            products that offer true value without any “window dressing.”</p>
            </div>
          </div>

        </div>

      </div>
    </section>
  </Layout>
)

export default StabilityPage
