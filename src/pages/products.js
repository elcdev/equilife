import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import ProductImage from '../images/products.png'

const ProductsPage = () => (
  <Layout>
    <SEO title='Products' />
    
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '70vh' }}>      
      <div className='carousel-inner'>
        <div className='item active'>          
          <img className='fill' src={ProductImage} alt='Equitable Life & Casualty'></img>
          <div className='carousel-caption'>
          </div>
        </div> 
      </div> 
    </div>     
    <section className='content-section'>
      <div className='container'>
        <div className='row'>          
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-shield-alt'></i> Product
                            Offerings</h3>
            <p className='section-subtitle'>Saving for retirement is increasing in importance as we are all
                            living longer.</p>
            <p className='section-text'>Interest rates and stock market prices fluctuate and cannot guarantee at
            what rate your savings will grow. By providing outstanding service and offering indexed
            annuity and insurance products that fill an important need, Equitable helps Americans
                            prepare for retirement and leave lasting legacies for their loved ones. </p>
          </div> 
        </div> 
      </div>
    </section>    
    <section className='content-section'>
      <div className='container'>
        <div className='row'>      
          <div className='col-md-4 feature text-center'>
            <div className='icon'>
              <i className='fa fa-piggy-bank'></i>
            </div>
            <h5 className='feature-title'>Fixed Index Annuities</h5>
            <p className='feature-desc'>Fixed Index Annuities (or 'FIAs') are designed to help you achieve your
                            goals for retirement savings.</p>
            <p>They provide insurance against major financial risks, such as market losses and outliving
                                your money.</p>
            <p><Link to='FIAs' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more
                                    about FIAs</Link></p>
          </div>
          <div className='col-md-4 feature text-center'>
            <div className='icon'>
              <i className='fa fa-hand-holding-usd'></i>
            </div>
            <h5 className='feature-title'>Multi-Year Guarantee Annuities</h5>
            <p className='feature-desc'>No matter what happens with the stock market, Multi-Year Guarantee
            Annuities (known as 'MYGs' or 'MYGAs') will earn a guaranteed compound rate of interest for
                            the 2- or 5-year guarantee period you choose. </p>
            <p><Link to='MYGs' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about
                                MYGAs</Link></p>
          </div>
          <div className='col-md-4 feature text-center'>
            <div className='icon'>
              <i className='fa fa-comments-dollar'></i>
            </div>
            <h5 className='feature-title'>Other Products</h5>
            <p>View information regarding Equitable's Medicare Supplement Insurance, Life Insurance, Cash
                            Plans, and additional offerings.</p>
            <p> </p>
            <br />
            <br />
            <p><Link to='additional' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Additional
                                Products</Link></p>
          </div>
        </div>
      </div>
    </section>


  </Layout >
)

export default ProductsPage
