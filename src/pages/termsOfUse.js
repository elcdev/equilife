import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

const TermsOfUsePage = () => (
  <Layout>
    <SEO title='Terms of Use' />
    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>Terms & Conditions</h3>
            <p className='section-main'>When you use our website, there may be times when you provide certain
            private and personal information about yourself to us. This may be especially true if you are a
            policyowner and you contact us wishing to access information concerning your coverage, a pending
            claim for benefits or to take advantage of various customer services that we can provide to you
            on our website.
            From the outset, we want you to know that we do respect your privacy. After all, we are in a
            business of trust, so keeping your personal information confidential and protected against
            unauthorized disclosure is very important to us.
            We intend to limit the amount of personal information needed by us to the minimum amount
            necessary to deliver the service you expect from us. While this will allow us to serve you
            better, at the same time it lets you know that we are not going to seek any more information
            than we will need to get the job done.
            To access our website, you will not need to provide us with any personal information about
            yourself. You will not be required to "log in" with a password or to register to use our
            website. Additionally, unless you contact us and give us certain information about yourself,
            your access and use of our website is anonymous. We only know how many "hits" we get on our
            website, but we do not know who is in fact using the site unless they tell us.
            We do provide a Contact Us feature on our website that does require you to provide to us certain
            personal information about yourself, like your name, address, policy number, telephone number or
            e-mail address. We will need this information to assist in any inquiry you may have and to
            determine the best way to respond to you to assure your needs have been met. Additionally, we
            may use this information to send you periodic information about us, our products and services,
            or topics of interest related to our products or business.
            You may wish to have more specific information about a particular product than what is provided
            to you on our website. In these instances, we may forward your request to a professional
            Equitable agent in your area to help you. We remind you that you are under no obligation to meet
            with an agent. But we have found, through decades of experience, that having a professional
            insurance representative in your own community that you can talk to is often the best way for
            you to get your questions answered and to assist you in your decision making.
            If you meet with a professional Equitable agent you may also find that you may disclose more
            personal information, usually health information, that guides you in determining if you can
            qualify to purchase a certain life or health insurance policy from us. Be assured that our agent
            representatives respect your privacy as well and will protect your personal information from
            unauthorized disclosure with the same degree of diligence that we do.
            Employees or agents who violate our privacy policy are subject to disciplinary action by us. We
            train all employees on HIPAA compliance and the importance of the privacy and confidentiality of
            all information we collect.
            We will maintain all personal information that you give to us in a secured database. You retain
            the right to "remove" this information from our database by simply contacting us and telling us
            to do so.
            We do not and will not share personal information you give to us with any unauthorized third
            party without your written permission. This means we do not sell or market our customer lists to
            anyone.
            There may be instances where we are compelled, by law, to disclose personal information provided
            to us. This would be, for instance, under a valid court order or subpoena, or at the request of
            insurance regulatory officials in their capacity as investigators or examiners. In any such
            instance, we would only disclose the information requested and would seek, through protective
            order or other legal means, the ability to keep this information confidential and not subject to
            disclosure to any other person.
            We are sure you have heard about situations where a "hacker" gains access to a business through
            a website and causes mischief by exposing customer accounts, credit card numbers, or financial
            information about the business. Guarding against being trespassed upon in this way is a matter
            we take seriously. While we cannot guarantee that this will never happen, we do employ various
            security measures that alert us to whether unauthorized access to us has been attempted - kind
            of like a burglar alarm system. We also use "firewalls" designed to accept what and whom we want
            accessing our system and to reject what we feel is unwanted or unauthorized access.
            Some websites attach a "cookie" onto your computer when you access the site. Cookies are
            commonly used to store information, like passwords, and can be generally helpful in your
            repeated use of a website. Cookies can also be used to set up a customer profile, which a
            business may market to another business, and result in your receiving advertisements or
            solicitations, whether you want them or not. As part of our total commitment to protecting your
            privacy, we do not engage in "online profiling" and when you access our website we will not send
            or attach a "cookie" to your computer at any time.
                    </p>
            <p>Whatever the case may be, we want and expect you to read the following Terms & Conditions, and to
            do so carefully. By accessing this website, you agree to be legally bound by them. But, if you
            do not agree to be bound by them, then we advise you to exit our website now without further
                        access to any of its pages.</p>
            <p>If you are an Equitable policyowner you will find a Customer Care Center on our website. This is
            designed to provide services and information to our policyowners, such as claims history, policy
            information, agent information, e-mail alerts and includes a message center. Our Customer Care
            Center requires a policyowner to log-in and have a password to access the Center. Errors can
            occur in the transmittal of information about a policy, its provisions, its premiums and claim
            history. Policyowners who access the Customer Care Center agree and understand that information
            provided to them does not establish a legal contract, is not guaranteed to be accurate and that
            the terms, provisions and conditions of their Equitable insurance policy will govern and
            control, and that we are not liable for any damages caused by errors in information provided to
            them via the Customer Care Center.
            While the Internet is a powerful tool, it is not without its flaws. You may have heard of, or
            read about, the real-life stories of "hackers" and the mischief they can cause; and, natural
            events or occurrences can easily disrupt Internet service as well. Moreover, there is always a
            concern about computer equipment itself, yours and ours - after all, these are machines, and
            something can go wrong. These things are matters normally outside and beyond our control. This
            is why we must "disclaim" any liability, to the extent we can under the law, for damages caused
            to you by your use of, or your inability to use, our website.
            Use of the Internet is solely at your own risk and is subject to all applicable state, national
            and international laws and regulations. While we endeavor to apply security protocols which are
            necessary and appropriate to the types of electronic communications which occur through use of
            our website, the confidentiality of any communication or material transmitted to or from us
            using our website, other websites or Internet electronic mail cannot be guaranteed, including
            personal information such as your address or social security number. We will not be liable for
            any loss resulting from a cause over which we have no direct control, including but not limited
            to, failure of electronic or mechanical equipment or communication lines, telephone or other
            interconnect problems, computer viruses, unauthorized access, theft, operator errors, severe
            weather, earthquakes, natural disasters, strikes or other labor problems, wars, or governmental
                        restrictions.</p>

            <p>YOUR USE OF OUR WEBSITE IS AT YOUR OWN RISK. EQUITABLE LIFE & CASUALTY INSURANCE COMPANY WILL NOT
            BE LIABLE, IN ANY WAY, FOR ANY DIRECT OR INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
            THAT MAY OCCUR OR ARISE FROM YOUR USE OF, OR YOUR INABILITY TO USE, OUR WEBSITE. ADDITIONALLY,
            THE INFORMATION CONTAINED ON OUR WEBSITE IS PROVIDED BY US TO YOU AS IS, AND WITHOUT ANY
            WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED. WE DO NOT WARRANT THAT THE FUNCTIONS CONTAINED IN
            THE SITE OR ANY MATERIALS WILL MEET ANY REQUIREMENTS OR NEEDS YOU MAY HAVE, OR THAT THE SITE OR
            ANY MATERIALS WILL OPERATE IN AN ERROR FREE OR UNINTERRUPTED FASHION, OR THAT ANY DEFECTS OR
            ERRORS IN THE SITE OR MATERIALS WILL BE CORRECTED, OR THAT THE SOFTWARE IS COMPATIBLE WITH ANY
            PARTICULAR PLATFORM. SOME JURISDICTIONS DO NOT ALLOW THE WAIVER OR EXCLUSION OF IMPLIED
                        WARRANTIES SO THEY MAY NOT APPLY TO YOU.</p>
            <p>We own our website. It is the exclusive property of Equitable Life & Casualty Insurance Company.
            We control the website and its content from our home office in Salt Lake City, Utah.
            We will periodically update the information on our website. Be aware that the information
            provided to you on our website is subject to change by us at any time, without notice to you or
            anyone else.
            Our website does not give you a complete description of all the terms, conditions, limitations
            or exclusions that might be applicable to any of our products or services. For more complete
            information, you may Contact Us directly or one of our professional Equitable agents in your
            area.
            We do not conduct business in every state or country. The products and services described in our
            website are not available in all jurisdictions, and not outside the United States.
            The product names, trademarks, service marks and logos we use in this website belong to us and
            are our property, unless otherwise indicated. They are protected by United States patent and
            copyright laws, as well as all other applicable federal or state laws.
            In this website, we give you permission to print a copy of the information contained on any page
            for your personal use only. Any other reproduction or use, by any means, of our product names,
            trademarks, service marks, logos, or our corporate name, without our express written consent, is
            prohibited. And remember, if you copy or reproduce any other logo, trademark, service mark or
            copyrighted information available to you through our website, you do so at your own risk,
            subject to the rights of the legal owners.
            Like other websites you may have visited, we may provide you with "links" to websites other than
            our own. We do so because we think these sites may have information or services of interest to
            you. Since the content in those websites is not under our control, we have no responsibility or
            liability for it. You visit those sites at your own risk, and subject to the terms and
            conditions of the owners. We do not endorse or promote any other website.
            As we pointed out earlier, you may wish to communicate directly with us through our website. If,
            and when you do so, please understand that we may share your communication with our employees or
            agents who are trained to answer your inquiry, and will, if need be, forward your communication
            to them for response to you. As such, your communication to us, to the extent it is not
            otherwise protected under federal or state privacy laws, is not considered confidential. We are
            free to use the information contained in your communication without limitation or liability.
            We do respect your privacy. The personal information that you provide to us through our website
            will be retained and used in strict accordance with our established Privacy Policy guidelines.

            Your agreement to be bound under these Terms & Conditions is governed under the laws of the
            State of Utah. If there is any dispute between you and us regarding your use of our website, or
            your breach of these Terms & Conditions, you agree such disputes will be subject to binding
            arbitration, under the rules of the American Arbitration Association, as the exclusive procedure
            for resolving any such disputes. Any arbitration proceedings will take place in Salt Lake City,
            Utah.
            You agree to indemnify us, our employees, officers, directors, shareholders and agents from and
            against all claims, damages, costs and expenses, including reasonable attorney fees, that are
            caused from your use of our website, whether done so through your negligent or intentional acts,
            or caused by your breach of these Terms & Conditions.
                    </p>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default TermsOfUsePage
