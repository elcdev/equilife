import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import ContactTree from '../images/contacttree.png'

const ContactPage = () => (
  <Layout>
    <SEO title='Contact' />

    
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '45vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={ContactTree} alt='Equitable Life & Casualty'></img>
          <div className='carousel-caption'>
          </div>
        </div> 
      </div> 
    </div> 
    <section className='content-section'>
      <div className='container'>
        <blockquote><i><b>*Please do not include personal health information in email inquiries.</b></i></blockquote>
        <div className='row'>
          <div className='col-md-4'>
            <div className='mb50 col-sm-6 col-md-12'>
              <h6 className='underline'><b>HOME OFFICE INFORMATION</b></h6>
              <p><b>Equitable Life & Casualty Insurance Company</b><br />
                    299 South Main Street <br />
                    Suite 1100 <br />
                    Salt Lake City, Utah 84111 <br />
                <b>Toll Free: (800) 352-5150</b><br />
                <i>Llame a nuestro: 877-579-3751</i><br />
                <br />
                <b>Our Business Hours:</b><br />
                <b>6:30am - 5:00pm MST Mon-Fri</b>
                <br />
                <small><i><b>LTC/Life</b></i> 8:00am-4:30pm MST<br /></small>
                <small><i><b>Underwriting</b></i> 6:30am-5:00pm MST<br /></small>
                <small><i><b>ENL Agency</b></i> 6:30am-5:00pm MST</small></p>
            </div>
            <div className='mb50 col-sm-6 col-md-12'>
              <h6 className='underline'><b>Equitable National Life (ENL)</b><br />
                <b>MedSupp Contacts</b></h6>
              <p><b>ENL MedSupp/Agency</b><br />
                  Phone: (888) 996-1619 <br />
                  Fax: (336) 759-3141 <br /></p>
              <p><b>ENL Claims Fax</b><br />
                  Fax: (801) 415-0953 <br /></p>
              <p><b>ENL Mailing Address</b><br />
                  PO BOX 11547<br />
                  Winston-Salem NC 27116<br /></p>
            </div>
          </div>
          <div className='col-md-4 feature text-left'>
            <h6 className='underline'><b>Annuity Contacts</b></h6>
            <p><b>Agency Services</b><br />
                  Phone: (800) 352-5121 <br />
                  Fax: (800) 579-3781 <br />
              <a href='mailto:Annuity.AgencyServices@equilife.com'><u>Annuity.AgencyServices@equilife.com</u></a>
            </p>
            <p><b>New Business</b><br />
                  Phone: (888) 352-5178 <br />
                  Fax: (888) 352-5126 <br />
              <a href='mailto:annuity.new.business@equilife.com'><u>Annuity.New.Business@equilife.com</u></a>
            </p>
            <p><b>Teton Service Center</b><br />
                  Phone: (888) 352-5122 <br />
                  Fax: (801) 994-1892 <br />
              <a href='mailto:annuity.pos@equilife.com'><u>Annuity.POS@equilife.com</u></a>
            </p>
            <p><b>MYGA Service Center</b><br />
                  Phone: (833) 889-0910 <br />
                  Fax: (833) 671-8870 <br />
              <a href='mailto:annuity.pos@equilife.com'><u>Annuity.POS@equilife.com</u></a>
            </p>
            <p><b>Annuity Mailing Address</b><br />
                  PO Box 30245 <br />
                  Salt Lake City, UT 84130-0245 <br />
            </p>
            <br />
          </div>
          <div className='col-md-4 feature text-left'>
            <h6 className='underline'><b>Equitable Life & Casualty (ELC)</b><br />
              <b>MedSupp Contacts</b></h6>
            <p><b>ELC MedSupp Service Center/Agency</b><br />
                  Phone: (888) 616-0017 <br />
                  Fax: (336) 759-3141 <br />
              <a href='mailto:Service@equilife.com'><u>Service@equilife.com</u></a></p>
            <p><b>ELC Mailing Address</b><br />
                  PO BOX 11642<br />
                  Winston-Salem NC 27116<br /></p>
            <h6 className='underline'><b>Additional Contacts</b></h6>
            <p><b>Hard Copy Annuity Brochure Requests</b><br />
                    Phone: (800) 352-5125 <br />
              <a href='mailto:supplyorders@equilife.com'><u>SupplyOrders@equilife.com</u></a></p>
            <p><b>California Consumer Privacy Act Contact</b><br />
                    Phone: (866) 916-7977 <br />
              <a href='mailto:CCPA@equilife.com'><u>CCPA@equilife.com</u></a></p>
            <p><b>Life Products</b><br />
                    Phone: (800) 352-5170 <br />
                    Fax: (801) 579-3765 <br /></p>
            <p><b>Long Term Care</b><br />
                    Phone: (888) 352-5124 <br /></p>
          </div>
        </div>
      </div> 
    </section>


  </Layout>
)

export default ContactPage
