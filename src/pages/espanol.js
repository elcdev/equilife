import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import EspanolImage from '../images/espanol.png'

const EspanolPage = () => (
  <Layout>
    <SEO title='Espanol' />

    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '50vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={EspanolImage} alt='Espanol' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>SE HABLA ESPANOL</h3>
            <p className='section-subtitle'>Hacemos todo lo posible para ayudar los miembros de la familia de
                            Equitable que hablan espanol.</p>
            <p className='section-subtitle'>Si tiene preguntas o necesita ayuda, llamenos gratis:</p>

            <p className='section-title'><b>1-877-579-3751.</b></p>
            <p className='section-subtitle'>Se le pedira dejar un mensaje, y un representante que hable espanol
                            le llamara lo mas pronto posible.</p>
          </div>
        </div>
      </div>
    </section>

  </Layout >
)

export default EspanolPage
