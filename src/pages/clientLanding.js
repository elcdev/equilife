import React from 'react'
import SEO from '../components/common/seo'

import DarkLogo from '../images/logo-dark.png'

const ProductsPage = () => (
  <>
    <SEO title='Client Landing' />
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
      integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous' />
    <nav className='navbar navbar-dark bg-primary' style={{ backgroundColor: '#30363d !important', height: '75px' }}>
    </nav>
    <br />
    <br />
    <br />
    <br />

    <div className='container'>
      <div className='col-md-12 card' style={{padding: '50px'}}>
      <img src={DarkLogo} alt='Equitable Life & Casualty' style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto', maxWidth: '100%', maxHeight: '100%', width: 'auto/9' }} />
        <br />
        <br />
        <br />
        <div className='row'>
          <div className='col-md-4'>
            <div className='card text-center' style={{minHeight: '200px !important'}}>
              <div className='card-header'>
                Life & Health Client Portal
                        </div>
              <div className='card-body'>
                <p className='card-text'>Our Life & Health clients can view & manage your policies here.</p>
                <br />
                <a href='https://secure.equilife.com/index.cfm' className='btn btn-primary' style={{borderColor: 'transparent'}}>Click Here</a>
              </div>
            </div>
          </div>
          <div className='col-md-4'>
            <div className='card text-center'>
              <div className='card-header'>
                Annuity Client Portal
                        </div>
              <div className='card-body'>
                <p className='card-text'>Access the Annuity Client Portal.</p>
                <br />
                <br />
                <a href='https://portal.equilife.com/' className='btn btn-primary' style={{borderColor: 'transparent'}}>Click Here</a>
              </div>
            </div>
          </div>
          <div className='col-md-4'>
            <div className='card text-center'>
              <div className='card-header'>
                Provider Login
                        </div>
              <div className='card-body'>
                <p className='card-text'>Health care providers can access Medicare Supplement policy information and more.</p>
                <a href='https://secure.equilife.com/provider-access/index.cfm' className='btn btn-primary' style={{borderColor: 'transparent'}}>Click Here</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer className='font-small fixed-bottom'>

      <div className='text-right py-1 px-3 small text-muted'>
        <a href='http://www.equilife.com/CCPA-PrivacyPolicy' target='_blank' rel='noopener noreferrer'>
          Privacy Policy
            </a>
        <br />
            ©<script type='text/javascript'>
          const today = new Date();
          const year = today.getFullYear();
          document.write(year.toString())
        </script>
            Equitable Life and Casualty
        </div>

    </footer>
  </>

)

export default ProductsPage
