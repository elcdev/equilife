import React from 'react'
import { Link } from 'gatsby'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

const CovidPage = () => (
  <Layout>
    <SEO title='Covid-19' />

    <br />
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>COVID-19 Update</h3>
            <p className='section-subtitle'>Current as of Friday, August 14 2020 09:49 AM MST. Information is
            updated as it is received.</p>
          </div> 
          <div className='col-md-12'>
            <p>
            We understand this is a very unsettling time for everyone; and we at
            Equitable Life & Casualty want to let you know we are thinking of you
            and hope you & your loved ones are safe and well.  
                        </p>
            <p>
            During this unprecedented time of uncertainty with the market, we
            remain committed to protecting you. We will always continue to be a
            long term, financially strong company you can trust.  
                        </p>
            <p>
            We continue to actively monitor the developments of the global spread
            of COVID-19 (novel coronavirus). The health and safety of our
            employees is paramount. To prevent the spread of the virus, we
            implemented our business continuity plan and transitioned to a workfrom-home plan on March 16th. We are happy to report we continue to
            have <strong>over 90%</strong> of our employees successfully working from the safety of
            their homes.  
                        </p>
            <p>
            For the few employees whose work requires a physical presence in our
            office, we have taken preventative measures, as advised by the CDC
            guidance, to ensure the safety of their health and well-being. 
                        </p>
            <p>
            We plan to continue operating this way for the foreseeable future. We
            will begin to transition our team back to our offices only when we are
            confident we are not putting anyone at risk. 
                        </p>
            <p>
            While there is a lot of uncertainty in the world today, there is one thing
            you can be certain of, we at Equitable Life are working hard everyday
            to serve your needs. Even though we are working in unusual
            circumstances, our commitment to excellence has not changed. We
            are currently operating within our normal service standards. This includes
            minimal hold times when you call us, prompt payment of claims and
            other withdrawal requests and timely processing of new business. 
                        </p>
            <p>
            We have also continued with our commitment to provide products that
            are of great importance when it comes to helping you prepare for
            retirement and products to help provide income you cannot outlive
            once you reach retirement. We encourage you to reach out to your
            agent to learn more about our suite of annuity products, which includes
            the Secure Savings&trade;, Teton&trade; and Denali&trade; lines. 
                        </p>
            <p>
            Equitable Life is committed to providing you superior customer service,
            and we are prepared to serve you. Thank you for your continued trust. 
                        </p>
          </div>
        </div> 
      </div>
    </section>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>COVID-19 New Jersey Response Order</h3>
            <p className='section-subtitle'>Current as of Friday, May 1 2020 09:19 AM MST. Information is
            updated as it is
                            received.</p>
          </div> 
          <div className='col-md-12'>
            <p>
              Pursuant to New Jersey Executive Order No. 123, Equitable has modified procedures to extend
              the Grace Period for remitting premium on any Medicare Supplement policy issued in New
              Jersey beyond the agreed upon due date from 31 days to 60 days as a result of the outbreak
              of COVID-19. No Medicare Supplement policies issued in New Jersey will be cancelled due to
              non-payment of premium during a period of no less than 60 days beyond the agreed upon
              premium due date. Additionally, any claims incurred during the extended Grace Period will
              not be denied or pended due to non-payment of premium during the extended Grace Period. The
              grace period may be initially applied towards the April or May premium as the policyholder
              determines and will continue for 60 calendar days from that date. After the 60-day emergency
              grace period, a policyholder must be offered the option of amortizing any unpaid premium
              over the remaining months of the policy, but for not less than six months.
                        </p>
          </div>
        </div> 
      </div>
    </section>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>COVID-19 Update</h3>
            <p className='section-subtitle'>Current as of Wednesday, Apr 29 2020 01:12 PM MST. Information is
            updated as it is received.</p>
          </div>
          <div className='col-md-12'>
            <p>
              As the pandemic situation continues to evolve, our top priority is to keep everyone safe and
              keep our business functioning, including all the team members who must work from the office
              and are vital to keep the company operational. As an organization, we have shown that we can
              do this with most everyone working remotely; we’re proving it now, every day. We are
              succeeding due to everyone’s adaptation and dedication to our shared success. The resiliency
              and determination you all have shown is remarkable.
            </p>
            <p>
              The Equitable response team is working to support and improve our current work configuration
              and plan for the path ahead. The outbreak trend is not yet declining in any of the regions
              where we have offices. Even when the pandemic begins to subside, the COVID-19 risk will
              remain and all of us need to continue to be vigilant. It is not a short-term problem, but
              rather a new risk we all will have to manage.
            </p>
            <p>
              The response team has been and will continue to analyze data, assess the situation,
              incorporate governmental guidance, and make decisions that are the best for your safety, our
              organization, and our team. At present, we cannot determine an exact date that remote
              working team members may begin to return to our offices full time, but we do not expect the
              process to start before June 1, 2020. For the foreseeable future, we will be operating in
              our current diffused configuration.
            </p>
            <p>
              When that time does come, we expect it to be an incremental process. This will best allow us
              to manage risk and keep our vital business activities operating. The process to establish
              our new normal will not be like a switch, where everything immediately resumes. It will be
              much like a dial that is turned slowly as the situation allows us to take actions safely.
            </p>
            <p>
              While the procedures are yet to be finalized, some characteristics of our new normal may be:
            </p>
            <ul>
              <li>• Extending the work from home policy to a broader group of team members, beyond the
              people who were working from home before onset of the pandemic</li>
              <li>• Staff returning to the office in stages, with new desk arrangements in place</li>
              <li>• Additional safety measures remaining in place, including physical distancing
              procedures and continuation of extra cleaning measures</li>
            </ul>
            <p>
              At present, IT will continue to bolster remote work configurations for all of our teams. The
              Technical Services team is currently working on acquiring additional webcams to facilitate
              face to face conversations for everyone, enhancing functionality for call center staff, and
              quickly solving technical issues as they arise.
            </p>
            <p>
              Expect more frequent updates from this point moving forward. We will work to communicate
              details of establishing our new normal as they are solidified.
            </p>
            <p> As an ongoing reminder, please follow these protective measures to reduce the novel
                coronavirus spread: </p>
            <ul>
              <li>• Practice physical distancing in the workplace. Keep at least six feet away from other
                                employees.</li>
              <li>• Practice good hygiene practices such as regularly washing your hands with soap and
              water
                                for 20 seconds.</li>
              <li>• Cover your cough or sneeze with a tissue, then throw the tissue in the trash.</li>
              <li>• Avoid touching your eyes, nose, and mouth.</li>
              <li>• Clean and disinfect frequently touched objects and surfaces using a regular household
                                cleaning spray or wipe.</li>
              <li>• Read or refresh your knowledge by learning more about the CDC's updated
              recommendations at www.cdc.gov or https://slco.org/health/COVID-19/.</li>
            </ul>

          </div>
        </div> 
      </div> 
    </section>
    
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>COVID-19 Update</h3>
            <p className='section-subtitle'>Current as of Monday, Mar 23 2020 04:03 PM MST. Information is
            updated as it is
                    received.</p>
          </div> 
          
          <div className='col-md-12'>
            <p>
              We have completed the primary roll out of our response plan, including work from home
              configurations for. As of today, we successfully have 85% of our employees setup and
              successfully working from home, with all key business operations fully functioning.
            </p>
            <p>
              At present, we are often experiencing higher than normal hold times due to call volume to
              our service centers. We appreciate your patience as we give every call the attention it
              deserves. While we work in decrease hold times, we encourage using our websites the email
              address below to contact when possible.
            </p>
            <p>
              We will continue to assess the situation and share more information as it becomes available.
                    Please visit <a href='https://www.equilife.com/'>www.equilife.com</a> for the latest
                    updates.
            </p>
            <p>
              For annuity agents and annuity clients, please email <b>Annuity.POS@equilife.com</b> with
                    questions.
            </p>
            <p>
              For life & health agents and life & health clients, please email <b>Service@EquiLife.com</b>
                    with
                    questions.
                </p>
          </div>
        </div> 
      </div> 
    </section>

    
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>COVID-19 Update</h3>
            <p className='section-subtitle'>Current as of Tuesday, Mar 17 2020 06:11 PM MST. Information is
            updated as it is
                    received.</p>
          </div> 
          
          <div className='col-md-12'>
            <p>
              We continue to actively monitor the spread of COVID-19 and we successfully have 70% of our
              employees working from the safety of their homes. As we make the final push to get as many
              of our employees to work from home, we will be temporarily shutting down our phone lines
              this evening starting at 4:00 pm MST. This excludes our few employees whose work requires a
              physical presence in our office. Our phone lines will be back up tomorrow morning.
                </p>
            <p>
              <strong>Annuity Telephonic Sales Utilizing Our Electronic Application (eApp)</strong>
                    These volatile times are a great reminder of the benefits provided with fixed annuities. If
                    clients want a fixed/indexed annuity but do not want to meet in person, then we have a great
                    solution! If you are using Firelight, you can take the app over the phone with the client.
                    All signatures and acknowledgments are done electronically. <strong>You do not need to
                        physically meet with the client!</strong> We are happy to answer any questions –
                    especially if this is the first time that you’ve used Firelight.
                </p>
            <p>
              We will continue to assess the situation and share more information as it becomes available.
                    Please visit <a href='https://www.equilife.com/'>www.equilife.com</a> for the latest
                    updates.
                </p>
            <p>
              For annuity agents and annuity clients, please email <b>Annuity.POS@equilife.com</b> with
                    questions.
                </p>
            <p>
              For life & health agents and life & health clients, please email <b>Service@EquiLife.com</b>
                    with
                    questions.
                </p>
          </div>
        </div> 
      </div> 
    </section>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline'>Equitable Life's Response to COVID-19</h3>
            <p className='section-subtitle'>Current as of Tuesday, Mar 17 2020 02:31 PM MST. Information is
            updated as it is
                    received.</p>
          </div> 
          
          <div className='col-md-12'>
            <p>
              As you are aware, COVID-19 (coronavirus) is spreading nationally and there have been
              confirmed
              cases in Salt Lake City, Utah and other cities where we have office locations. However, none
              of
              our
              employees have contracted the virus at this time. We are closely monitoring the evolving
              COVID-19 situation and understand this is a very unsettling time for everyone. We are
              committed
              to providing superior services to our agents, customers and business partners to ensure the
              continuity of our business operations.
                </p>

            <p>
              The health and safety of our employees is paramount to Equitable Life. We are following the
              guidance provided by the Centers for Disease Control and Prevention (CDC) as well as local
              and
              state public health authorities, to safeguard the health and well-being of all our
              employees. In
              order to mitigate the potential spread, we are implementing a work from home plan starting
              this
              Monday, March 16. Due to the success of our 2019 flex work program, our employees are
              extremely
              capable of working offsite and have the proper technology and tools required to complete
              their
              work responsibilities, including our claims, underwriting, new business, service and
              operations
              teams. For the few employees whose work requires a physical presence in our office, we have
              taken preventative measures to ensure the safety of their health and well-being.
                </p>

            <p>
              During this transition, we expect phone calls to increase which will impact our hold times.
              We
              encourage you to utilize our portals and email for status updates.
                </p>

            <p>
              While the steps we are taking are extreme, we believe the actions are necessary and will
              help
              ensure we are in the best position to provide uninterrupted services while protecting the
              health
              of our employees.
                </p>
            <p>
              We will continue to assess the situation and share more information as it becomes available.
                </p>
            <p>
              For annuity agents and annuity clients, please email <b>Annuity.POS@equilife.com</b> with
                    questions.
                </p>
            <p>
              For life & health agents and life & health clients, please email <b>Service@EquiLife.com</b>
                    with
                    questions.
                </p>
          </div>
        </div> 
      </div> 
    </section>

    
    <section className=''>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 mt40 mb40'>
            <div className='cta-3'>
              <h3 className='title'>Talk To Us!</h3>
              <p className='desc'>Call us because we're awesome and would love to help!</p>

              
              <Link to='contact' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Contact Us</Link>
              <div className='clearfix'></div>
              <span className='far fa-paper-plane bg-icon icon-rotate'></span>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          
          <div className='col-md-3 col-sm-6'>
            <div className='counter-box text-center'>
              <div className='count-info'>
                <h6><span className='counter'>80</span>+</h6>
                <p><b>years in business.</b></p>
              </div> 
            </div> 
          </div> 
          <div className='col-md-3 col-sm-6'>
            <div className='counter-box text-center'>
              <div className='count-info'>
                <h6><span className='counter'>500,000</span>+</h6>
                <p><b>delighted clients.</b></p>
              </div> 
            </div> 
          </div> 
          <div className='col-md-3 col-sm-6'>
            <div className='counter-box text-center'>
              <div className='count-info'>
                <h6><span className='counter'>7,000</span>+</h6>
                <p><b>cups of coffee.</b></p>
              </div> 
            </div> 
          </div> 
          <div className='col-md-3 col-sm-6'>
            <div className='counter-box text-center'>
              <div className='count-info'>
                <h6><span className='counter'>180</span>+</h6>
                <p><b>happy Home Office employees!</b></p>
              </div> 
            </div> 
          </div> 

        </div> 
      </div> 
    </section>



  </Layout>
)

export default CovidPage
