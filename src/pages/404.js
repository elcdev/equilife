import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

const NotFoundPage = () => (
  <Layout>
    <SEO title='404: Not found' />

    <br />
    <br />
    <br />
    <br />
    <section style={{ margin: 20 }}>
      <h1>Not Found</h1>
      <p>The requested URL was not found on this server.</p>
    </section>
  </Layout>
)

export default NotFoundPage
