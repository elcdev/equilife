import { Link } from 'gatsby'
import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'
import '../sass/equilife/main.css'

import ProductImage from '../images/DenaliCrop.jpg'
import DenaliImage from '../images/Denali-tagline-blue.png'
import EquitableImage from '../images/ELC-Logo-white-text.png'

import DenaliBrochureImage from '../images/DenaliBrochure.jpg'
import DenaliBonusBrochureImage from '../images/DenaliBonusBrochure.jpg'

import DenaliProductHighlightSheetCoverImage from '../images/DenaliProductHighlightSheet.jpg'
import DenaliBonusProductHighlightSheetCoverImage from '../images/DenaliBonusProductHighlightSheet.png'
import DenaliRecruitingRateSheetCoverImage from '../images/DenaliRecruitingRateSheet.png'
import ProductAvailabilityGridCoverImage from '../images/ProductAvailabilityGrid.jpg'
import DenaliRecruitingAd1CoverImage from '../images/DenaliRecruitingAd1.jpg'
import DenaliRecruitingAd2CoverImage from '../images/DenaliRecruitingAd2.jpg'
import AtlasRightPlace from '../images/AtlasRightPlace.jpg'
import ELCFinancialStrength from '../images/ELCFinancialStrength.jpg'
import DenaliFAQ from '../images/DenaliFAQ.png'

import DenaliBrochFinal from '../assets/Denali-Broch-final.pdf'
import DenaliBonusBrochFinal from '../assets/Denali-BONUS-Broch-final.pdf'

import AtlasRightPlaceGenericFINAL from '../assets/Atlas Right Place_generic[FINAL].pdf'
import DenaliBonusProductHighlightSheet from '../assets/Denali Bonus Product Highlight Sheet.pdf'
import DenaliProductHighlightSheet from '../assets/Denali Product Highlight Sheet.pdf'
import DenaliAgentAd1 from '../assets/Denali-AgentAd1.pdf'
import DenaliAgentAd2 from '../assets/Denali-AgentAd2.pdf'
import ELCFinStrengthEOY2019 from '../assets/ELCFinStrength-EOY2019.pdf'
import NewProductAvailabilityGrid from '../assets/New Product Availability Grid.pdf'
import RateSheetDenali from '../assets/Rate Sheet Denali.pdf'
import DenaliFAQPDF from '../assets/FAQ Policyholder Name Change.pdf'

// import '../../sass/denali/_denali.sass'
import '../sass/denali/_denali.sass'

const Denali = () => (
    <React.Fragment>
        <div id='slider' className='carousel slide carousel-style-4' style={{ height: '70vh' }}>
            <div className='carousel-inner'>
                <div className='item active'>
                    <img className='fill' src={ProductImage} alt='Equitable Life & Casualty'></img>
                    <div className='carousel-caption'>
                    </div>
                </div>
            </div>
        </div>
        <img className='fill denali-logo-image' src={DenaliImage} alt='Equitable Life & Casualty'></img>
        <img className='fill' src={EquitableImage} style={{ width: '25rem', height: 'auto', position: 'absolute', right: '35px', top: '45px', bottom: '54px' }} alt='Equitable Life & Casualty'></img>
        <section className='content-section'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 section-header text-center'>
                        <div className='container'>

                            <div className='row'>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '1.5rem', margin: 0, fontWeight: '900', lineHeight: '150%' }}>
                                        DO YOU FEEL UNCERTAIN ABOUT RETIREMENT?
                                    </h5>
                                </div>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '1.5rem', margin: 0, fontWeight: '900', lineHeight: '150%' }}>
                                        DO YOU HAVE A RETIREMENT INCOME STREAM THAT YOU CAN'T OUTLIVE?
                                    </h5>
                                </div>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '1.5rem', margin: 0, fontWeight: '900', lineHeight: '150%' }}>
                                        DO YOU HAVE A PLAN THAT CAN PROTECT YOU FROM SURPRISES?
                                    </h5>
                                </div>

                            </div>

                            <div className='row'>

                                <div className='col-md-4 feature text-center'>
                                    <p className='feature-desc' style={{ color: 'black', margin: 0, fontSize: '1.35rem', fontWeight: '900' }}><bold>It's almost impossible not to.</bold></p>
                                </div>

                                <div className='col-md-4 feature text-center' >
                                    <p className='feature-desc' style={{ color: 'black', margin: 0, fontSize: '1.35rem', fontWeight: '900' }}>Most don't.</p>
                                </div>

                                <div className='col-md-4 feature text-center'>
                                    <p className='feature-desc' style={{ color: 'black', margin: 0, fontSize: '1.35rem', fontWeight: '900' }}>It's okay if you don't. We are here to help!</p>
                                </div>

                            </div>
                            <div className='row'>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '5rem', margin: 0, fontWeight: '600' }}>80%</h5>
                                    <p className='feature-desc' style={{ color: 'black', fontSize: '1.35rem' }}>of Americans are nervous about retirement.</p>
                                </div>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '5rem', margin: 0, fontWeight: '600' }}>73%</h5>
                                    <p className='feature-desc' style={{ color: 'black', fontSize: '1.35rem' }}>of Americans are concerned about not having a lifetime income.</p>
                                </div>

                                <div className='col-md-4 feature text-center'>
                                    <h5 className='feature-title' style={{ color: '#3A6874', fontSize: '5rem', margin: 0, fontWeight: '600' }}>71%</h5>
                                    <p className='feature-desc' style={{ color: 'black', fontSize: '1.35rem' }}>of Americans are concerned about the risk of a financial surprise.</p>
                                </div>

                            </div>
                        </div>
                        <p className='section-subtitle' style={{ marginBottom: '5px' }}>
                            Equitable’s Denali™ Series provides guarantees where you need guarantees and flexibility where you need flexibility. The right combination ensures you are ready for life’s uncertainties and leads to a
                        </p>
                        <h3 className='section-title' style={{ marginBottom: '5px', fontWeight: '700' }}>
                            <bold>retirement: Elevated.</bold>
                        </h3>
                        <p className='section-text' style={{ color: '#ccc', fontSize: '1rem' }}>
                            https://insurancenewsnet.com/innarticle/80-of-americans-nervous-about-retirement-nest-egg-study#.XqtNBmhKiUk
                        </p>
                        <hr style={{ border: '0', margin: '0', width: '100%', height: '3px', background: '#000' }}></hr>
                    </div>
                </div>
            </div>
        </section>

        <section className='content-section'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 section-header text-center'>
                        <div className='container'>
                            <h3 className='section-title' style={{ marginBottom: '5px', fontWeight: '700' }}>
                                <bold>Learn more by clicking below:</bold>
                            </h3>
                            <div className='row'>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliBrochFinal} target="_blank">
                                        <img className='fill' src={DenaliBrochureImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliBonusBrochFinal} target="_blank">
                                        <img className='fill' src={DenaliBonusBrochureImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliProductHighlightSheet} target="_blank">
                                        <img className='fill' src={DenaliProductHighlightSheetCoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliBonusProductHighlightSheet} target="_blank">
                                        <img className='fill' src={DenaliBonusProductHighlightSheetCoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href="https://portal.equilife.com/" target="_blank">
                                        <img className='fill' src={DenaliRecruitingRateSheetCoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href="https://portal.equilife.com/" target="_blank">
                                        <img className='fill' src={ProductAvailabilityGridCoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliAgentAd1} target="_blank">
                                        <img className='fill' src={DenaliRecruitingAd1CoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={DenaliAgentAd2} target="_blank">
                                        <img className='fill' src={DenaliRecruitingAd2CoverImage} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                {/* TODO: This was commented out per Marketing/Stephani request as we have not been given authorization to post it publicly yet */}
                                {/* <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href={AtlasRightPlaceGenericFINAL} target="_blank">
                                        <img className='fill' src={AtlasRightPlace} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div> */}

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href="http://media.equilife.com/annuity/ELCFinStrength-EOY2019.pdf" target="_blank">
                                        <img className='fill' src={ELCFinancialStrength} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                                <div className='col-md-4 feature text-center' style={{ margin: '20px 0 20px 0' }}>
                                    <a href="http://media.equilife.com/annuity/FAQ Policyholder Name Change.pdf" target="_blank">
                                        <img className='fill' src={DenaliFAQ} style={{}} alt='Equitable Life & Casualty'></img>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </React.Fragment >
)

export default Denali
