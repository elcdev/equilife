import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import SLCImage from '../images/slc.png'
import SILAC from '../images/SILAC logo.png'
import AllLogos from '../images/all company logos.png'

class LeadershipPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isVision: true };
  }

  switchMessage(id) {
    var leadershipButtons = document.getElementsByClassName("leadership");
    Array.from(leadershipButtons).forEach(element => {
      if (element.id === id) {
        element.setAttribute('class', 'leadership active');
      } else {
        element.setAttribute('class', 'leadership');
      }
    });
    id += 'Description';
    var leadershipDescriptions = document.getElementsByClassName("description");
    Array.from(leadershipDescriptions).forEach(element => {
      if (element.id === id) {
        element.setAttribute('class', 'description tab-pane active');
      } else {
        element.setAttribute('class', 'description tab-pane hide');
      }
    });

  }


  render() {
    return (
      <Layout>
        <SEO title='Leadership Team' />

        <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
          <div className='carousel-inner'>
            <div className='item active'>
              <img className='fill' src={SLCImage} alt='Leadership Team' />
              <div className='carousel-caption'>
              </div>
            </div>
          </div>
        </div>

        <section className='content-section'>
          <div className='container'>
            <div className='row'>


              <div className='col-md-12 section-header text-center'>
                <h3 className='section-title underline longer-underline'><i className='fas fa-mountain'></i> Leadership Team
                    </h3>
                <div className='section-main'>
                  <p>Equitable Life & Casualty Insurance Company was founded in 1935 and is Utah’s oldest active
              life insurer.</p>
                  <div className='image'> <img className='image' src={SILAC}
                    style={{ width: 'auto/9', maxWidth: '25%', maxHeight: '100%' }} alt='silac logo' /></div>
                  <br />
                  <p>SILAC, Inc. purchased Equitable in April of 2017, bringing both access to capital and
                  strong, experienced leadership.
                                <br />Equitable, Equitable National, Sterling Investors Life, and Triad Insurance
                                Administrators are all proudly owned by SILAC, Inc.</p>
                  <div className='image'> <img className='image' src={AllLogos}
                    style={{ width: 'auto/9', maxWidth: '85%', maxHeight: '100%' }} alt='all logos' />
                    <p></p>
                    <br />
                    <br />
                  </div>
                  <div className='col-md-12 section-header text-center'>
                    <h3 className='section-title underline longer-underline'> Our Leaders</h3>
                  </div>

                  <div className='col-md-12 col-md'>
                    <div className='tab-style-9'>
                      <ul className='nav nav-tabs'>
                        <li id='sHilbert' role='presentation' className='leadership active'><button onClick={() => this.switchMessage('sHilbert')} >
                          <span className='icon outline'></span> Stephen C. Hilbert</button>
                        </li>
                        <li id='nCuneo' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('nCuneo')} >
                          <span className='icon outline'></span> Ngaire E. Cuneo</button>
                        </li>
                        <li id='jAdams' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('jAdams')} >
                          <span className='icon outline'></span> James S. Adams</button>
                        </li>
                        <li id='gAcker' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('gAcker')} >
                          <span className='icon outline'></span> G. Daniel Acker</button>
                        </li>
                        <li id='sMatthews' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('sMatthews')} >
                          <span className='icon outline'></span> Scott D. Matthews</button>
                        </li>
                        <li id='sBelnap' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('sBelnap')} >
                          <span className='icon outline'></span> Samuel D. Belnap</button>
                        </li>
                        <li id='eDzierzon' role='presentation' className='leadership' ><button onClick={() => this.switchMessage('eDzierzon')} >
                          <span className='icon outline'></span> Erica Dzierzon</button>
                        </li>
                      </ul>


                      <div className='tab-content mb20'>
                        <div role='tabpanel' className='description tab-pane active' id='sHilbertDescription'>
                          <h5 className='mb20'><b>Stephen C. Hilbert,</b></h5>
                          <h5 className='mb20'>Chief Executive Officer</h5>
                          <div className='section-main'>Mr. Hilbert is currently the Chairman, CEO, and
                          President of SILAC, Inc., an insurance holding company he formed in
                          2015. SILAC is focused on the Life Insurance industry and marketing
                          Life, Annuity and Supplemental Health products in all states except New
                          York. SILAC owns Equitable Life & Casualty, Equitable National Life,
                          both Utah Life Insurance companies, and Triad Insurance Administrators,
                          a third party administration business, all of which were acquired in
                          April 2017. SILAC also purchased Sterling Investors Life as its original
                          acquisition in August of 2015. Focused on organic growth and strategic
                          acquisitions, in less than three years, SILAC increased its assets under
                          management from approximately $17 Million to over $400 Million.
                                                <p>From 1979 to 2000, Mr. Hilbert was President, Chairman of the Board,
                            and Chief Executive Officer of Conseco, Inc. (“Conseco”). Mr.
                            Hilbert founded Conseco with $10,000 to capitalize on an opportunity
                            he identified in the life insurance industry. Mr. Hilbert took
                            Conseco public in 1985, when it had $100 Million of assets. In 1986,
                            he took Conseco from the NASDAQ to the New York Stock Exchange. In
                            1994, Mr. Hilbert was CEO of four New York Stock Exchange listed
                            companies. Under Mr. Hilbert’s leadership, Conseco grew to a NYSE
                            financial services company with $23 Billion of equity market capital
                                                    and $100 Billion of assets under management.</p>
                            <p>Mr. Hilbert formed two life insurance specific funds targeting life
                            insurance acquisitions and developing life and health products. CCP
                            was formed in 1990 and was one of the best performing funds where
                            investors received over 100% IRR. CCP was distributed in 1994. CCPII
                            was the second insurance fund formed in 1994. Investors received
                            approximately 80% IRR on the fund, which was distributed in 1995.
                                                </p>
                                                Mr. Hilbert also formed and managed a private equity fund, MH Equity,
                                                from 2005 through 2013.
                                            </div>
                          <p>Mr. Hilbert helped endow a Baccalaureate insurance program at Indiana
                          State University, where he served as a Director of the Indiana State
                          University Foundation. He also was a member of the Board of Governors
                          for the Indianapolis Museum of Art and the Board of Directors of the St.
                          Vincent Hospital Foundation, the Indianapolis Zoo and the Indianapolis
                                                Convention and Visitors’ Association.</p>
                          <p>Through 2005 Mr. Hilbert served as Director of Vail Resorts Inc., a New
                          York Stock Exchange Company, which owns Vail, Beaver Creek, Breckenridge
                          and Keystone Ski Resorts. He served as a trustee of Park Tudor School,
                          the Central Indiana Council on Aging Foundation, and the Marion County
                          (Indiana) Sheriff’s Police Athletic League. He has chaired corporate
                                                campaigns for the United Way of Hamilton County (Indiana).</p>
                          <p>Mr. Hilbert has been recognized as one of the leaders in giving back to
                          the Indianapolis community, including the Hilbert Circle Theatre, home
                          of the Indianapolis Symphony Orchestra, the Hilbert Conservatory, at the
                          Indianapolis Zoo, the Hilbert Early Education Center at Park Tudor, the
                          Hilbert Pediatric Emergency Room at St. Vincent Hospital and numerous
                                                other worthy organizations which benefit the Indianapolis community.</p>
                        </div>
                        <div role='tabpanel' className='description tab-pane hide' id='nCuneoDescription'>
                          <h5 className='mb20'><b>Ngaire E. Cuneo,</b></h5>
                          <h5 className='mb20'>Executive VP of Corporate Development</h5>
                          <p className='section-main'>Ms. Cuneo is the Executive Vice President for SILAC,
                          EFIG and all the insurance companies. She also serves on the Board of
                          Directors. Ms. Cuneo’s focus is on the strategic development of the
                          companies through organic growth and mergers and
                          acquisitions. Ms. Cuneo is a member of the Company’s Investment
                          Committee. Prior to joining Sterling Investors, Ms. Cuneo spent over 35
                                                years in the financial services sector, including private equity.</p>
                          <p>Before joining SILAC, Inc., Ms. Cuneo was a partner in the Devlin Group,
                          LLC, a firm involved in the acquisition of insurance and financial
                          technology companies. She led the negotiation and purchase of
                          Forethought Financial Services, Inc., which specializes in pre-need and
                          annuity type products. She was also responsible for the NxLight
                          acquisition, a company that services the insurance sector, amongst
                                                others, with a secured electronic signature product.</p>
                          <p>From 1992 through 2001, she was an Executive Vice President of Corporate
                          Development for Conseco, Inc. and President of Conseco Capital Partners,
                          the alternative equity subsidiary of Conseco. Ms. Cuneo was instrumental
                          in the formation of Conseco Capital Partners II, L.P., an acquisition
                          partnership formed with $624 Million of commitments from 35 limited
                          partners including the California Public Employee Retirement System and
                                                the State of Michigan Pension Fund.</p>
                          <p>Prior to Conseco, Ms. Cuneo was a corporate officer of General Electric
                          Capital Corp. At GE Capital she was responsible for over 40 transactions
                          including $2 Billion in leveraged buyouts of insurance companies. Ms.
                          Cuneo started her career with the General Accounting Office both in New
                                                York City and Washington, DC.</p>
                          <p>During the Reagan administration, Ms. Cuneo was selected as a
                          Presidential Executive Exchange Fellow. She graduated with honors from
                          the College of New Rochelle and received an MBA from Iona College. She
                          attended the Executive Development Program at the Wharton School of the
                                                University of Pennsylvania.</p>
                          <p>Currently Ms. Cuneo serves on the Board of Directors of Duke Realty
                                                    Corporation, a leader in commercial real estate.</p>
                        </div>
                        <div role='tabpanel' className='description tab-pane hide fade' id='jAdamsDescription'>
                          <h5 className='mb20'><b>James S. Adams,</b></h5>
                          <h5 className='mb20'>Vice Chairman & Treasurer</h5>
                          <p>James S. Adams is Board member and Vice Chairman of Finance for SILAC,
                          EFIG and all the insurance companies. Prior to joining SILAC and EFIG,
                          Mr. Adams spent over 30 years in the financial services field, including
                          private equity. From 1986 through 2002, he was a senior financial
                          executive for Conseco, Inc., and from 1992 thru 2002 he was Senior Vice
                          President, Treasurer and Chief Accounting Officer. During his tenure, he
                          participated in the acquisition of over 20 financial services companies
                          and the formation of two acquisition partnerships (Conseco Capital
                                                Partners I and II, L.P.) focusing on the financial services sector.</p>
                          <p>In addition, from 1992 through 2002, as an executive of Conseco Private
                          Capital Group, he participated in numerous transactions in a variety of
                          sectors including gaming, entertainment venues, broadcasting, telecom,
                          and real estate. As Treasurer of Conseco he directed the issuance of
                          over $20 Billion in capital market transactions including senior bank
                          facilities, public debt, securitized assets of all classes and types,
                          preferred and common equity. Mr. Adams graduated from Purdue University
                                                with a Bachelor of Science in General Management.</p>
                        </div>
                        <div role='tabpanel' className='description tab-pane hide fade' id='gAckerDescription'>
                          <h5 className='mb20'><b>G. Daniel Acker,</b></h5>
                          <h5 className='mb20'>President & Chief Marketing Officer</h5>
                          <p>Mr. Acker is President and Chief Marketing Officer of Equitable Life &
                          Casualty Insurance Company, Equitable National Insurance Company, Inc.,
                          and Sterling Investors Life Insurance Company. He also serves on the
                          Board of Directors. Before joining Equitable, Mr. Acker served as
                          President of Sentinel Security Life Insurance Company. While at
                          Sentinel, Mr. Acker was responsible for growing the employee base from
                          13 to over 150 and assets from $50 Million to over $800 Million. This
                          growth was a result of Mr. Acker's leadership in creating several new
                          products for the senior market, including Medicare Supplement, Fixed and
                          Index Annuities, and Hospital Indemnity plans. Mr. Acker also expanded
                                                the agent distribution network to over 16,000 appointed agents.</p>
                          <p>During this time, he also successfully started a third-party
                          administrator and an independent marketing organization. Prior, Mr.
                          Acker worked for Educators Mutual Insurance rising to become Assistant
                                                Vice President of Finance.</p>
                          <p>Mr. Acker is a member and past President of the Utah Chapter of Insurance
                          Accounting and Systems Association (“IASA”). He is also a board member
                          for the Life Insurers Council and holds an MBA from Westminster College
                          in Salt Lake City and a Bachelors’ degree in Finance from the University
                                                of Utah.</p>
                        </div>
                        <div role='tabpanel' className='description tab-pane hide fade' id='sMatthewsDescription'>
                          <h5 className='mb20'><b>Scott D. Matthews,</b></h5>
                          <h5 className='mb20'>Chief Legal Officer</h5>
                          <p>Scott Matthews is a Board Member and Chief Legal Officer for SILAC, EFIG
                          and all insurance companies. Mr. Matthews is an attorney with 20 years
                          of experience in both private practice and in-house General Counsel. Mr.
                          Matthews has represented clients in complex business transactions and
                          litigation. Mr. Matthews directs and supervises all legal and regulatory
                          compliance matters for the combined SILAC entities, including corporate
                          governance, regulatory filings, litigation, reinsurance agreements,
                          third-party administration contracts and marketing agreements, among
                          other things. Mr. Matthews graduated summa cum laude from Indiana
                          University School of Law – Indianapolis. He earned his undergraduate
                          degree from Indiana University with a B.A. in Telecommunications and a
                          minor in Business. He is a member of the Association of Life Insurance
                                                Counsel.</p>
                        </div>
                      </div>
                      <div role='tabpanel' className='description tab-pane hide fade' id='sBelnapDescription'>
                        <h5 className='mb20'><b>Samuel D. Belnap,</b></h5>
                        <h5 className='mb20'>Chief Information Officer</h5>
                        <p>Mr. Belnap has over 23 years of experience in Information Technology and Systems Management for
                        numerous companies across many industries. His current focus is on Information Technology
                        governance and compliance including strategic planning, business alignment, regulatory review,
                        process review & recommendations, and IT audit.</p>
                        <p>Mr. Belnap has a proven ability to intuitively identify and integrate business, IT and external
                        requirements in a simplified and sustainable approach. He has extensive tactical implementation
                        experience with strategic plan execution, transparency metrics, regulatory compliance design,
                        business alignment, IT assessments and organizational changes.</p>
                      </div>
                      <div role='tabpanel' className='description tab-pane hide fade' id='eDzierzonDescription'>
                        <h5 className='mb20'><b>Erica Dzierzon,</b></h5>
                        <h5 className='mb20'>Assistant Vice President of Finance</h5>
                        <p>Ms. Dzierzon is the Assistant Vice President of Finance for Equitable Life & Casualty Insurance
                        Company. She holds a bachelor’s degree in Accounting, a master’s degree in Business
                        Administration and is a Certified Public Accountant.</p>
                        <p>She has over 15 years of experience in Accounting and Finance in various industries including
                        high-growth startups and public accounting.</p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </section>

      </Layout>
    )
  }
}

export default LeadershipPage
