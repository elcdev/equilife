import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import FIAImage from '../images/FIAs.png'
import TetonBonus from '../images/teton bonus- gray blue.png'
import TetonSeries from '../images/teton series-gray blue.png'
import TetonBanner from '../images/Teton Consumer Ad header.png'
import TetonBrochure from '../assets/Teton Brochure.pdf'
import TetonBonusBrochure from '../assets/Teton Bonus Brochure.pdf'

import DenaliBanner from '../images/Denali w tagline-blue.png'
import DenaliBonus from '../images/denali bonus opt4.png'
import DenaliSeries from '../images/DENALI W ELC.png'
import DenaliBrochure from '../assets/Denali-Broch-final.pdf'
import DenaliBonusBrochure from '../assets/Denali-BONUS-Broch-final.pdf'

const FIAPage = () => (
  <Layout>
    <SEO title='Fixed Index Annuities' />

    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={FIAImage} alt='Fixed Index Annuities' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-piggy-bank'></i> Fixed
                            Index Annuities</h3>
            <div className='col-md-12 section-header text-center'>
              <p className='section'>Saving for retirement is becoming increasingly important as we are all
              living longer. Interest rates and stock market prices fluctuate and cannot guarantee at
              what rate your savings will grow. Annuities are designed to help you achieve your goals
              for retirement savings. They provide insurance against major financial risks, such as
                                market losses and outliving your money.</p>
              <p className='section-subtitle'>Both of our FIAs are simple, easy-to-understand designs,
              without the burden of fees or cumbersome riders. Both series offer 7-, 10- and 14-year
                                versions.</p>
              <div className='image'>
                <img className='image' src={DenaliBanner}
                  style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '50%', width: 'auto/9;' }}
                  alt='teton header logo' />
                  <br/>
                  <br/>
                <p className="section-subtitle">Equitable’s Denali™ Series provides guarantees where you need guarantees and flexibility where you
                                        need flexibility. The right combination ensures you are ready for life’s uncertainties and leads to a RETIREMENT: ELEVATED.</p>
                <p className="section-subtitle">Denali is North America’s tallest mountain.  It grows higher and faster than most mountains in the world and is still growing! Its location makes it more resistant to erosion – the perfect symbol of a product focused on retirement accumulation that won’t be eroded by fees.</p>
                <p className="section-subtitle">Our Denali™ Series provides both accumulation <i>and</i> lifetime income.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-6 feature text-center'>
            <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '60%', width: 'auto/9;' }}>
              <img className='img-thumbnail' src={DenaliSeries} alt='teton' />
            </div>
            <p></p>

            <p><b>The Denali™ Series is our product suite that specializes in providing both accumulation and lifetime income.</b></p>
            <br />
            <p>It's a modified single premium fixed indexed annuity series with 7-, 10- and 14-year versions, so that you can find the term that best suits your needs. It automatically comes with one fixed interest and 8 indexed crediting strategies.</p>

            <p>Annual free withdrawal, lifetime withdrawal, wellness withdrawal, home health care, nursing care, terminal illness and wealth transfer benefits are all automatically included with this product line for no charge.*</p>
            <p><a href={DenaliBrochure}
              className='st-btn primary-btn hvr-back hvr-sweep-to-right'>
                Learn more about the Equitable Denali™ Series
            </a></p>
          </div>
          <div className='col-md-6 feature text-center'>
            <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '40%', width: 'auto/9;' }}>
              <img className='img-thumbnail' src={DenaliBonus} alt='teton' />
            </div>
            <p></p>

            <p><b>The Denali™ Bonus Series is our product suite that provides the perfect combination of upfront premium bonus paired with accumulation and lifetime income.</b></p>

            <p>It's a modified single premium fixed indexed annuity series with 7-, 10- and 14-year versions, so that you can find the term that best suits your needs. It automatically comes with one fixed interest and 8 indexed crediting strategies.</p>
            <p>Premium bonus, annual free withdrawal, lifetime withdrawal, wellness withdrawal, home health care, nursing care, terminal illness and wealth transfer benefits are all automatically included with this product line for no charge.*</p>
            <p><a href={DenaliBonusBrochure}
              className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about Equitable Denali™ Bonus</a></p>
          </div>
          <div className='col-md-12 section-header text-center'>
            <p className='feature-desc'><i>*Not all riders are available in every state.</i></p>
          </div>
        </div>
      </div>
    </section>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-12 section-header text-center'>
            <div className='col-md-12 section-header text-center'>
              <div className='image'>
                <img className='image' src={TetonBanner}
                  style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '100%', width: 'auto/9;' }}
                  alt='teton header logo' />
                <p className='section'>The Tetons are a relatively young mountain range in Wyoming that are
                still growing. In the annuity world, we'd say they are still accumulating. What a
                                    perfect name to tie our flagship accumulation product to! </p>

                <div className='col-md-6 feature text-center'>
                  <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '75%', width: 'auto/9;' }}>
                    <img className='img-thumbnail' src={TetonSeries} alt='teton' />
                  </div>
                  <p></p>

                  <p><b>The Teton™ Series is our product suite that specializes in accumulation.</b></p>
                  <br />
                  <p>It's a modified single premium fixed indexed annuity series with 7-, 10- and 14-year versions, so that you can find the term that best suits your needs. It automatically comes with one fixed interest and 8 indexed crediting strategies.</p>

                  <p>Annual free withdrawal, cumulative withdrawal, home health care, nursing care, terminal illness and wealth transfer benefits are all automatically included with this product line for no charge.*
                        </p>
                  <p><a href={TetonBrochure}
                    className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about the Equitable
                                Teton Series</a></p>
                </div>
                <div className='col-md-6 feature text-center'>
                  <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '75%', width: 'auto/9;' }}>
                    <img className='img-thumbnail' src={TetonBonus} alt='teton' />
                  </div>
                  <p></p>

                  <p><b>The Teton™ Bonus Series is our product suite that provides the perfect combination of
                                upfront premium bonus paired with accumulation. </b></p>

                  <p>It's a modified single premium fixed indexed annuity series with 7-, 10- and 14-year versions, so that you can find the term that best suits your needs. It automatically comes with one fixed interest and 8 indexed crediting strategies.</p>
                  <p>Premium bonus, annual free withdrawal, cumulative withdrawal, home health care, nursing care, terminal illness and wealth transfer benefits are all automatically included with this product line for no charge.*</p>
                  <p><a href={TetonBonusBrochure}
                    className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about Equitable Teton Bonus</a></p>
                </div>
                <div className='col-md-12 section-header text-center'>
                  <p className='feature-desc'><i>*Not all riders are available in every state.</i></p>
                  {/* <p className='section-sub-sub-subtitle'>**The 'S&P 500®' is a product of S&P Dow Jones Indices LLC,
                  a division of S&P Global, or its affiliates (“SPDJI”), and has been licensed for use by
                  Equitable Life & Casualty Insurance Company (“Equitable”). Standard & Poor’s® and S&P® are
                  registered trademarks of Standard & Poor’s Financial Services LLC, a division of S&P Global
                  (“S&P”); Dow Jones® is a registered trademark of Dow Jones Trademark Holdings LLC (“Dow
                  Jones”); and these trademarks have been licensed for use by SPDJI and sublicensed for
                  certain purposes by Equitable. Equitable’s Teton Fixed Indexed Annuities are not sponsored,
                  endorsed, sold or promoted by SPDJI, Dow Jones, S&P, or their respective affiliates, and
                  none of such parties make any representation regarding the advisability of investing in such
                  product(s) nor do they have any liability for any errors, omissions, or interruptions of the
                            S&P 500®.</p> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default FIAPage
