import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import WorkForUsImage from '../images/workforus.png'

const RepELCPage = () => (
  <Layout>
    <SEO title='Represent ELC' />

    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={WorkForUsImage} alt='Represent ELC' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-users'></i> Be A Part of
                            our team</h3>
            <p className='section-subtitle'>A Place Where Caring, Professional Agents Find A Home</p>
            <p className='section-text'>We believe in and vigorously support the independent agency system and
            we've done it since 1935. We know that nothing can replace the friendly, personal and
            professional service an Equitable agent provides to our policyowners. We know it's true,
                            because our policyowners tell us so. We're very proud of that.</p>
            <p className='section-text'>Agents interested in becoming contracted and appointed with Equitable
                            Life & Casualty should call the Agency Department at 1-800-352-5121 or <a
                href='mailto:agencyservices@equilife.com' title='Contact the Agency Department'>email
                                us.</a></p>

          </div>
        </div>
      </div>
    </section>

  </Layout>
)

export default RepELCPage
