import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import AdditionalImage from '../images/additional2.png'

const AdditionalPage = () => (
  <Layout>
    <SEO title='Additional Products' />

    <div id='slider' className='carousel slide carousel-style-4' style={{height:'60vh'}}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={AdditionalImage} alt='Additional Products' />
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-comments-dollar'></i>Additional Products</h3>
            <div className='col-md-12 section-header text-center'>
              <p className='section-sub-subtitle'><i>At this time, Equitable is not offering any of the
                  following products for sale; but we have provided information for our existing
                  policyowners as a reference. <br/>
                  If you are interested in learning more about these products, please contact us and
                  we will review the options that may be offered by one of our affiliate insurance
                  carriers.</i>
              </p>
              <br />
              <div className='section-title underline longer-underline'/>
              <br />
              <p className='section-subtitle'>Medicare Supplement</p>
              <p>Medicare is a federal government health care program enacted in 1965. Often referred to
                  today as "Original Medicare," the program is primarily set up to provide medical expense
                  coverage to persons over age 65. The Medicare program is administered by the Centers for
                  Medicare and Medicaid Services (CMS), which is a division of the U.S. Department of
                  Health and Human Services.
                  The Medicare program is divided into two parts: Part A, for hospital and skilled nursing
                  facility expenses... and Part B, for medical expenses such as doctor, outpatient
                  facility and home health care charges. Each part has its own set of deductibles,
                  co-payments and pre-approved charges, which normally change each year.
              </p>
              <p><i>Does Medicare cover all costs?</i></p>
              <p>Most, but not all. There are certain co-payments and deductibles you must pay. Medicare
                  pre-assigns allowable charges for all kinds of illnesses, surgeries and procedures. When
                  the cost of a medical procedure is more than Medicare's 'approved' charge, you have to
                  pay the difference. This is why there are private insurance options, like Medicare
                  Supplement Insurance, to help pay for the expenses Medicare does not cover.</p>
              <p>If you have a Medicare Supplement policy with Equitable, please click on the link below
                  to learn more about the USA Managed Care Organization and to find a participating
                  hospital in your area.</p>
              <p><Link to='usaManageCare' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>USA Managed Care Organization</Link></p>
            </div>
            <br />
              <div className='section-title underline longer-underline'/>
            <br />
            <p className='section-subtitle'>Life Insurance </p>
            <p>Life insurance is designed to protect against the financial risk associated with your death.
                It may also help you provide to your loved ones the financial resources and security you
                want for their future. Think of the common needs associated with the purchase of life
                insurance: mortgage and debt protection, funds to pay any final expenses (medical and/or
                funeral) or estate taxes, income for your spouse and family, the care of a child with
                'special needs,' or perhaps the funding needed for the education of a child or grandchild.
                If these are concerns in your life, then a life insurance policy may be something you should
                consider.
            </p>
            <br />
              <div className='section-title underline longer-underline'/>
            <br />
            <p className='section-subtitle'>Cash Benefit Plans</p>
            <p>Cash Benefit Plans, as we like to call them at Equitable, are legally recognized as limited
                benefit or supplemental insurance coverages. Because Cash Benefit Plans are primarily
                considered to be supplemental insurance, the benefits these plans pay are most always paid
                in addition to any other insurance, including Medicare. Most, if not all, individual and
                group major medical plans 'coordinate' benefits with other insurance; even long-term care
                policies coordinate with Medicare, meaning that if Medicare pays for covered services then
                the policy will not pay benefits for the same covered services.
            </p>
            <p>Therein is the beauty of Cash Benefit Plans. Unlike Medicare and Medicare Supplement
                insurance, where benefits are usually assigned to the providers, the benefits from Cash
                Benefit Plans are not paid to the hospital, the doctor or the nursing home - they are paid
                directly to the policyowner to use however and for any purpose they see fit. Yes, they can
                help to reimburse for those deductibles and co-pays not covered by insurance; but Cash
                Benefits Plans are not designed primarily for that purpose.
            </p>


          </div>

        </div>
      </div>
    </section>
  </Layout>
)

export default AdditionalPage
