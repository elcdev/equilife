import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import MainPageImage from '../images/mainpage3.png'

const WorkingHerePage = () => (
  <Layout>
    <SEO title='Working Here' />
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={MainPageImage} alt='Working Here' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-home'></i> Come Work With
                Us!</h3>
            <p className='section-subtitle'>Since 1935, Equitable has been proud to be a family owned and
            operated company, committed to caring. Our dedicated teamwork, family perspective,
            traditional high standards, and insistence on being the best have earned Equitable a
            reputation as a caring leader. We support change, personal development and career
            advancement by promoting within the company, and giving each member of our team the
                opportunity to excel with the latest advancements in training and development.</p>
            <p className='section-subtitle'>With a simple philosophy of rewarding excellence and commitment, we
            offer each employee all the flexibility you would expect in a family atmosphere, plus other
                outstanding benefits.</p>
            <h3 className='section-title underline longer-underline'><i className='fa fa-list-ol'></i> Current
                Openings:</h3>
            <p className='section-subtitle'>To view & apply for open positions, please click <a
              href='https://equilife.applicantpro.com/jobs/' target='_blank' rel="noopener noreferrer"><u>here.</u></a></p>
            <p className='section-text'><i>When it's time to select an employer, consider becoming a part of The
                    Equitable Family.</i></p>
            <p className='section-text'>-Equitable Life & Casualty is an Equal Opportunity Employer-</p>
          </div>
        </div>
      </div>
    </section>


  </Layout >
)

export default WorkingHerePage
