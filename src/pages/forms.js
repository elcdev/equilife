import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import FormsImage from '../images/common forms.png'

const FormsPage = () => (
  <Layout>
    <SEO title='Common Forms' />
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '50vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={FormsImage} alt='Common Forms' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-file-signature'></i> Common Forms</h3>
            <p className='section'>Please click on the links below to download the PDF forms.</p>
          </div>
          <section className='content-section' style={{ marginLeft: 'auto', marginRight: 'auto' }}>
            <div className='col-md-6 feature text-center' style={{ width: '50%', float: 'left' }}>
              <p className='feature-title'><b>ANNUITY-SPECIFIC FORMS:</b></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/request_to_change_beneficiary.pdf'
                target='_blank' rel='noopener noreferrer' title='Name Change'><b><u> Request to Change Beneficiary</u></b></a></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/annuitant_ownership_service_form.pdf'
                target='_blank' rel='noopener noreferrer' title='Address Change'><b><u> Annuitant-Ownership Service Form</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/death_of_owner.pdf'
                target='_blank' rel='noopener noreferrer' title='Annuity Death Claim'><b><u> Annuity Death Claim Form</u></b></a></p>
              <br />
              <p className='feature-title'><b>OTHER PRODUCT REQUESTS & CHANGES:</b></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/change_name.pdf'
                target='_blank' rel='noopener noreferrer' title='Name Change'><b><u> Name Change</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/change_address.pdf'
                target='_blank' rel='noopener noreferrer' title='Address Change'><b><u> Address Change</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/change_beneficiary.pdf'
                target='_blank' rel='noopener noreferrer' title='Name Change'><b><u> Beneficiary Change</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/ownership_change.pdf'
                target='_blank' rel='noopener noreferrer' title='Owner Change'><b><u> Change of Ownership</u></b></a></p>
              <br />
              <p className='feature-title'><b>BILLING & OTHER SELF-HELP OPTIONS:</b></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/bank_draft_authorization.pdf'
                target='_blank' rel='noopener noreferrer' title='Auto Bank'><b><u> Auto Bank Draft (with Direct Bill Premium Notice)</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/loan_surrender_form.pdf'
                target='_blank' rel='noopener noreferrer' title='Policy Loans'><b><u> Policy Loans</u></b></a></p>
              <p className='feature-link'><a href='contact' target='_blank' rel='noopener noreferrer' title='Suspension'><b><u>
                Suspension of Benefits During Medicaid Eligibility</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/loan_surrender_form.pdf'
                target='_blank' rel='noopener noreferrer' title='Surrender'><b><u> Surrender Life Policy for Cash Value</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/ownership_change.pdf'
                target='_blank' rel='noopener noreferrer' title='cancel death'><b><u> Cancel Due To Death</u></b></a></p>
            </div>
            <div className='col-md-6 feature text-center' style={{ width: '50%', float: 'left' }}>
              <p className='feature-title'><b>GENERAL CLAIM FORMS:</b></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/pay_providers_direct.pdf' target='_blank' rel='noopener noreferrer'
                title='Pay Provider Direct'><b><u> Pay Provider Direct</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/do_not_assign.pdf'
                target='_blank' rel='noopener noreferrer' title='Stop Pay Provider Direct'><b><u> Stop Pay Provider Direct</u></b></a></p>
              <p className='feature-link'><a href='https://media.equilife.com/annuity/HIPPA_authorization.pdf'
                target='_blank' rel='noopener noreferrer' title='HIPAA Authorization Designation'><b><u> HIPAA Authorization Designation</u></b></a></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/health_information_authorization.pdf'
                target='_blank' rel='noopener noreferrer' title='Health Information Authorization'><b><u> Health Information Authorization</u></b></a></p>
              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/claims_deposit_authorization.pdf' target='_blank' rel='noopener noreferrer'
                title='Pay Provider Direct'><b><u> Claims Deposit Authorization</u></b></a></p>
              <br />
              {/* <p className='feature-title'><b>HOMECARE, FAMILY CAREGIVERS:</b></p> */}
              <p className='feature-title'><b>LONG-TERM CARE CLAIMS FORMS:</b></p>


              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/Claims Initiation Kit.pdf'
                target='_blank' rel='noopener noreferrer' title='Claims Initiation Kit'><b><u> Claims Initiation Kit</u></b></a></p>

              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/Claims Initiation Form.pdf'
                target='_blank' rel='noopener noreferrer' title='Claims Initiation Form'><b><u> Claims Initiation Form</u></b></a></p>

              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/LTC Facility Claim Form.pdf'
                target='_blank' rel='noopener noreferrer' title='LTC Facility Claim Form'><b><u> LTC Facility Claim Form</u></b></a></p>

              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/Care by Family Member Application.pdf'
                target='_blank' rel='noopener noreferrer' title='Care by Family Member Application'><b><u> Care by Family Member Application</u></b></a></p>

              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/Private Caregiver Application.pdf'
                target='_blank' rel='noopener noreferrer' title='Private Caregiver Application'><b><u> Private Caregiver Application</u></b></a></p>

              <p className='feature-link'><a
                href='https://media.equilife.com/annuity/Caregiver Time Records and Notes.pdf'
                target='_blank' rel='noopener noreferrer' title='Caregiver Time Records and Notes'><b><u> Caregiver Time Records & Notes</u></b></a></p>

              <br />
            </div>

          </section>
        </div>
      </div>
    </section>

  </Layout>
)

export default FormsPage
