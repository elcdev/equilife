import React from 'react'
import SEO from '../components/common/seo'
import { Helmet } from 'react-helmet'

import DarkLogo from '../images/logo-dark.png'
import privacyStyles from '../sass/equilife/privacy.module.scss'

const CCPAPrivacyPolicyPage = () => (
  <>
    <SEO title='CCPA Privacy Policy' />
    <Helmet>
      <html className={privacyStyles.privacyPage}></html>
    </Helmet>

    <div className={privacyStyles.privacyContainer}>
		  <img src={DarkLogo} alt="logo" className={`${privacyStyles.logo} ${privacyStyles.mb20}`}/>
		  <h2 className={privacyStyles.mb80}>Equitable Life & Casualty Insurance Company and our affiliates, Equitable National Life Insurance Company and Sterling Investors Life Insurance Company (collectively, “Company” or “we”).</h2>
		
		  <p>May 2020--</p>
		  <h1 className={`${privacyStyles.centered} ${privacyStyles.mb30}`}>Privacy Policy</h1>
		
      <p>In This Privacy Policy, we will address:</p>
      <ul>
        <li><a className={privacyStyles.blackLink} href="#collect">Personal Information We Collect</a></li>
        <li><a className={privacyStyles.blackLink} href="#use">How We Use Personal Information</a></li>
        <li><a className={privacyStyles.blackLink} href="#share">How We Share Personal Information</a></li>
        <li><a className={privacyStyles.blackLink} href="#children">Children</a></li>
        <li><a className={privacyStyles.blackLink} href="#third-party">Third Party Sites</a></li>
        <li><a className={privacyStyles.blackLink} href="#security">Security</a></li>
        <li><a className={privacyStyles.blackLink} href="#california">Additional Privacy Information for California Residents</a></li>
        <li><a className={privacyStyles.blackLink} href="#glba">GLBA Notice</a></li>
      </ul>
      <p className={privacyStyles.mb50}>The Company takes privacy issues seriously and we are committed to protecting and respecting the privacy and security of your personal information.  This Privacy Policy describes our practices in connection with information that we collect through this website and when you directly apply for insurance from us.</p>
		
      <h3 id="collect" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Personal Information We Collect</h3>
      <h4 className={privacyStyles.bold}>Information collected directly from you</h4>
      <p>We intend to limit the amount of personal information needed by us to the minimum amount necessary to deliver the service you expect from us.  The types of Personal Information we collect depends on the product you have with us, or about which you have inquired, or the services we provide.  This information can include identifying information about you such as your name, address, and date of birth, as well as information about your prior insurance policies, certain health information, in accordance with applicable laws, and insurance claims history.</p>
      <h4 className={privacyStyles.bold}>Information collected from third parties</h4>
      <p>We also obtain information about you from third parties such as consumer and insurance reporting organizations and health care providers.  This information may include, but is not limited to, your health history; please note, these organizations may retain information provided by us and disclose it to others.</p>
      <h4 className={privacyStyles.bold}>Information we collect automatically</h4>
      <p>When you access our websites  we or our third party suppliers use a variety of technologies, such as cookies and web beacons, that automatically collect data (such as, without limitation, your device type, browser type, internet protocol address, operating system used, number of visits, average time spent on the site, and pages viewed).  This data is used to operate the websites more efficiently, maintain the security of your online session, and improve our website design.</p>
      
      <h3 id="use" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>How We Use Personal Information</h3>
      <p>The Company uses your personal information to respond to your requests and inquiries, to process and administer your application, to provide you with services and insurance products in which you enroll and to process claims.  The Company may also use your personal information to offer you other Company products or services that may be of interest to you.  The Company will use your personal information for other purposes as permitted by law, including to protect your interest and ours in the detection, prevention and mitigation of fraud, to meet the requirements of applicable laws and regulations, to comply with requests of regulators with jurisdiction over our services, and for product development and delivery.</p>

      <h3 id="share" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>How We Share Personal Information</h3>
      <p>
        We limit the information we share and the parties we share it with.  What we share depends on the types of products or services we provide and the applicable laws.  We may share all or part of the Personal Information about you that we collect with affiliated and unaffiliated companies, and others such as law enforcement, courts, and government agencies, as allowed by law.  For example:
        <ul>
          <li className={privacyStyles.mb20}>We may share your Personal Information with third party service providers to enable them to provide services such as policy or claim administration, payment processing, order fulfillment, website hosting, customer service, email delivery service, auditing service, and other similar services.</li>
          <li className={privacyStyles.mb20}>We may share your Personal Information with insurance agents and brokers for purposes of fulfilling your requests and addressing your insurance needs.</li>
          <li className={privacyStyles.mb20}>We may share your Personal Information with our agents to offer our products and services to you more effectively.</li>
          <li className={privacyStyles.mb20}>We may share your Personal Information with other recipients including, for example, our HIPAA Business Associates, affiliates, claims representatives, consumer and insurance reporting agencies, law enforcement, actuarial or research entities, and government agencies.</li>
        </ul>
        Other disclosures of your personal information, subject to applicable law, include disclosures:
        <ul  className={privacyStyles.mb30}>
          <li>To a policyholder or authorized representative provide information about the status of a transaction.</li>
          <li>To persons acting in a fiduciary or representative capacity on your behalf.</li>
          <li>To protect against or prevent actual or potential fraud, unauthorized transactions, claims or other liability.</li>
          <li>To protect the confidentiality or security of our records pertaining to you, the service or product, or the transaction therein.</li>
          <li>For institutional risk control, or for resolving customer disputes or inquiries.</li>
          <li>To provide information to persons assessing our compliance with industry standards, and our attorneys, accounts and auditors.</li>
          <li>In connection with a proposed or actual sale, merger, transfer or exchange of all or part of our business.</li>
          <li>To a government agency to determine your eligibility for benefits they may have to pay for or to answer a question from the government agency.</li>
          <li>To consumer and insurance reporting organizations (such as your credit, financial or health history; please note, these organizations may retain information provided by us as permitted by applicable law).</li>
          <li>For purposes otherwise permitted or required by law.</li>
        </ul>
        In addition, we may share Personal Information about our former customers in the manner described above.
      </p>
      
      <h3 id="children" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Children</h3>
      <p>The Company will not knowingly collect Personal Data from children under the age of 16 without seeking prior parental consent if required by applicable law. We will only use or disclose Personal Data about a child to the extent permitted by law, to seek parental consent pursuant to local law and regulations or to protect a child. The definition of "child" or "children" should take into account applicable laws.</p>

      <h3 id="third-party" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Third Party Sites</h3>
      <p>This Privacy Policy does not address, and we are not responsible for, the privacy, information gathering, or other practices of any third parties, including any third party operating any site even if we provide a link.  The inclusion of a link on this side does not imply endorsement of the linked site by us or our affiliates.</p>

      <h3 id="security" className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Security</h3>
      <p>We seek to use reasonable organizational, technical, and administrative measures to protect Personal Information under our control. Unfortunately, no data transmission over the Internet or any data storage system can be guaranteed to be 100 percent secure. If you have reason to believe that your interaction with us is no longer secure, please contact us at the address below.</p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Questions about policy</h3>
      <p>We seek to give you choices regarding our use and disclosure of your Personal Information.  If you have questions about our privacy policy or information practices, you can contact us at:</p>
      <p className={privacyStyles.mb50}>
        Equitable Life & Casualty <br />
        Attn:  Legal Department <br />
        299 South Main Street, Suite 1100 <br />
        Salt Lake City, Utah 84111 <br />
        1-800-352-5150
      </p>
      
      <h1 id="california" className={privacyStyles.mb20}>For California Residents – California Consumer Privacy Act Notice</h1>
      <h1 className={privacyStyles.mb20}>May 2020</h1>
      <p>The California Consumer Privacy Act of 2018 (CCPA) gives California consumers the right to know what Personal Information is collected about them, and how it will be used, disclosed, and sold. The CCPA also gives California consumers the right to request access to and deletion of their personal information, and the right to request that their personal information not be sold.</p>
      <p>The purpose of this notice is to provide consumers with a description of our practices regarding the collection, use, disclosure, and sale of Personal Information and instructions for submitting CCPA data privacy requests. <strong>Much of the Personal Information that we collect, use, and disclose may be exempt from the CCPA because it is regulated by other federal and state laws that apply to insurance companies; but because we are concerned about privacy issues, we want you to be familiar with how we collect, use, and disclose your Personal Information.</strong></p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Collection of Personal Information</h3>
      <p>The types of Personal Information we collect, use, and disclose depend on your relationship with us.  If the nature of your relationship with us changes, an additional privacy notice may apply and will be provided to you.  We have collected in the preceding 12 months or may collect in the future the following categories of Personal Information about California consumers.</p>
      
      <div className={`${privacyStyles.table} ${privacyStyles.fsSmall} ${privacyStyles.mb20}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.vbottom} ${privacyStyles.bold} ${privacyStyles.centered}`}>Types of Personal Information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30} ${privacyStyles.vbottom} ${privacyStyles.bold} ${privacyStyles.centered}`}>Examples</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.vbottom} ${privacyStyles.bold} ${privacyStyles.centered}`}>Primary Purpose for Collection</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20}`}>Identifying information about you</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30}`}>Identifiers such as name, postal address, internet protocol (IP) address, email address, account name, Social Security number, or other similar identifiers including signature, physical characteristics or description, telephone number, insurance policy number, bank account number, any other financial information, medical information, or health insurance information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50}`}>To respond to your requests and inquiries, process and administer your application, provide you with services and insurance product in which you enroll, and process claims.  </div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20}`}>Characteristics of protected classifications under California or federal law </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30}`}>Gender, marital status, age, health information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50}`}>To process and administer your application and provide you with services and insurance product in which you enroll.</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20}`}>Commercial information </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30}`}>Records of other insurance products or services purchased or claims history with other insurers</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50}`}>For institutional risk control, or for resolving customer disputes or inquiries, to persons acting in a fiduciary or representative capacity on your behalf, to provide information to persons assessing our compliance with industry standards, and our attorneys, accounts and auditors, to protect the confidentiality or security of our records pertaining to you, the service or product, or to market other Company products and services that may be of interest to you.</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20}`}>Internet or other electronic network activity information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30}`}>Information regarding a consumer’s interaction with an internet website, application, or advertisement, browser information, web analytics, including IP address, time of visit, page(s) visited, cookies, pixel tags, and other similar technologies Filtering IP Addresses and phone call area code information for fraud prevention and other associated purposes</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50}`}>To operate the website more efficiently, maintain the security of your online session,  improve our website design, protect against or prevent actual or potential fraud, unauthorized transactions, claims or other liability, and to protect the confidentiality or security of our records pertaining to you, the service or product.</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20}`}>Audio, electronic, visual, thermal, olfactory, or similar information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col30}`}>Customer service call recordings</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col50}`}>To protect against or prevent actual or potential fraud, unauthorized transactions, claims or other liability.</div>
        </div>
      </div>
      
      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>How We Collect Personal Information</h3>
      <p>
        We collect the Personal Information in the chart above from the following categories of sources:
        <ul>
          <li>From you and your transactions with us, such as when you apply for insurance, pay insurance premiums, file an insurance claim, or give us your contact information</li>
          <li>From our affiliates</li>
          <li>From others, including consumer reporting agencies</li>
          <li>From publicly available sources, such as government records</li>
        </ul>
      </p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Why We Collect Personal Information</h3>
      <p>We collect the Personal Information as described in the chart above for the following purposes: as we believe to be necessary or appropriate: (a) under applicable law,   (b) to comply with legal process; (c) to respond to requests from public and government authorities,   (d) to enforce our terms and conditions; (e) to protect our operations or those of any of our affiliates; (f) to protect our rights, privacy, safety, or property, and/or that of our affiliates, you, or others; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</p>
      <p>We may combine the Personal Information we collect for the above purposes. We will not collect additional categories of Personal Information, nor use the Personal Information collected for additional purposes, without providing you with additional notice.</p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>What Personal Information We Sell or Share</h3> 
      <p>The CCPA defines “sale” very broadly to include sharing or disclosing of personal information with a third party.  There are a number of circumstances where we are allowed to share personal information with third parties which are not considered to be a sale.  For example, we may share personal information with service providers who work on our behalf if the service provider agrees not to use your personal information for other purposes.</p>
      <p>We do not sell Personal Information for revenue-generating purposes.  We do not sell personal information of minors under 16 years of age.</p>
      <p>We may share the Personal Information with third parties, including but not limited to, third party service providers, insurance agents and brokers, HIPAA Business Associates, affiliates, claims representatives, consumer and insurance reporting agencies, law enforcement, and government agencies.</p>     

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Your Rights</h3>
      <p>
        We may determine that the Personal Information related to your request is exempt from the CCPA. If your Personal Information is exempt, the CCPA does not require us to provide it to you or delete it, but we may provide information about the categories of Personal Information we collect, use, and disclose based on your relationship with us in response to your verifiable request.  In the interest of good customer relations, your rights are outlined below:
        <ul>
          <li>You have the right to request that we disclose what personal information we collect, use, and disclose.</li>    
          <li>You have the right to request deletion of any personal information about you that we have collected from you except as needed to comply with applicable insurance law.</li> 
          <li>We don’t sell your personal information so there’s no need to opt-out of the sale of such information.</li> 
          <li>You have a right not to receive discriminatory treatment by us for your exercise of your privacy rights.</li> 
        </ul>
      </p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>How to Submit a Data Privacy Request</h3>
      <p>To submit a request for access or deletion (to the extent we can do so), please use the following secure link to submit a form request: <a className={privacyStyles.regularLink} href="https://privacy.equilife.com/" target="_blank" rel="noopener noreferrer">https://privacy.equilife.com/</a>.  You can also submit a request by calling our dedicated toll-free number at 1-866-916-7977.</p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Verification of Your Identity</h3>
      <p>To process your request for access or deletion, we must be able to verify your identity to a reasonable degree of certainty. In order to do so, you must provide the required identifying information when completing the online request form or making a request through one of our customer service agents.  We will ask you to provide your contact information and an additional identifier based on your relationship with us.  Before we process your request, we will match these data points with data points we currently maintain to verify your identity and your relationship with us.</p>

      <h3 className={`${privacyStyles.bold} ${privacyStyles.underlined}`}>Responding to Your Requests</h3>
      <p>Please note that because much of the data we have is exempt from the CCPA, we are not required to provide you with access to certain information under the CCPA.</p>
      <p>In addition, we may not be required to delete information under certain circumstances.  For example, the CCPA includes exemptions that provide that we do not have to delete data that for example, is necessary to complete a transaction, detect security incidents, or for certain other internal purposes. You can designate an authorized agent to make a request to know or delete, on your behalf. When you use an authorized agent to submit a request for access or deletion, you must provide the authorized agent with written permission to do so, and, in certain circumstances, we may ask you to verify your own identity directly with us.</p>
      <p>We may deny a request from an authorized agent that does not submit proof that they have been authorized by you to act on your behalf.</p>

      <h4 className={privacyStyles.bold}>Contact for More Information</h4>
      <p className={privacyStyles.mb50}>For questions or concerns about our privacy policies and practices, please contact us at our secure link (<a className={privacyStyles.regularLink} href="https://privacy.equilife.com/" target="_blank" rel="noopener noreferrer">https://privacy.equilife.com/</a>) or at our toll-free number 1-866-916-7977.</p>

      <p className={`${privacyStyles.fsTiny} ${privacyStyles.mb80}`}>121108875.1</p>
      <h4 className={`${privacyStyles.bold} ${privacyStyles.centered} ${privacyStyles.mb80}`}>GLBA NOTICE FOLLOWS</h4>

      <p className={`${privacyStyles.fsTiny} ${privacyStyles.right}`}>Revised:  December 2019</p>
      <div id="glba" className={`${privacyStyles.table} ${privacyStyles.borderb} ${privacyStyles.mb10}`}>
        <div className={`${privacyStyles.row} ${privacyStyles.borderb}`}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.centered} ${privacyStyles.borderb} ${privacyStyles.fsLarge} ${privacyStyles.headerInvertedBackground} ${privacyStyles.pn}`}>FACTS</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.vtop} ${privacyStyles.bold} ${privacyStyles.borderb} ${privacyStyles.ptn}`}>WHAT DOES EQUITABLE DO WITH YOUR PERSONAL INFORMATION?</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.fsBig} ${privacyStyles.headerGrayBackground} ${privacyStyles.vmiddle}`}>Why?</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>Financial companies choose how they share your personal information. Federal law gives consumers the right to limit some but not all sharing. Federal law also requires us to tell you how we collect, share, and protect your personal information. Please read this notice carefully to understand what we do.</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.fsBig} ${privacyStyles.headerGrayBackground} ${privacyStyles.vmiddle}`}>What?</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            The types of personal information we collect and share depend on the product or service you have with us. This information can include:
            <ul className={`${privacyStyles.mn} ${privacyStyles.mb5}`}>
              <li>Social Security number and medical information</li>
              <li>Income and account balances</li>
              <li>Assets and employment information</li>
            </ul>
          </div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.fsBig} ${privacyStyles.headerGrayBackground}`}>How?</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>All financial companies need to share customers’ personal information to run their everyday business. In the section below, we list the reasons financial companies can share their customers’ personal information; the reasons Equitable chooses to share; and whether you can limit this sharing.</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.bold} ${privacyStyles.fsTiny} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern} ${privacyStyles.centered}`}>Reasons we can share your personal information</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.bold} ${privacyStyles.fsTiny} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern} ${privacyStyles.centered}`}>Does Equitable share?</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.bold} ${privacyStyles.fsTiny} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern} ${privacyStyles.centered}`}>Can you limit this sharing?</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}>
            <strong>For our everyday business purposes –</strong><br />
            such as to process your transactions, maintain your account(s), respond to court orders and legal investigations, or report to credit bureaus
          </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>Yes</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}>
            <strong>For our marketing purposes –</strong><br />
            to offer our products and services to you
          </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>Yes</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}><strong>For joint marketing with other financial companies</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>We don't share</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}>
            <strong>For our affiliates’ everyday business purposes -</strong><br />
            information about your transactions and experiences
          </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>Yes</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}>
            <strong>For our affiliates’ everyday business purposes -</strong><br />
            information about your creditworthiness
          </div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>We don’t share</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}><strong>For our affiliates to market to you</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>Yes</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>Yes</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col50} ${privacyStyles.fsTiny}`}><strong>For nonaffiliates to market to you</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>No</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col25} ${privacyStyles.fsTiny} ${privacyStyles.centered} ${privacyStyles.vmiddle}`}>We don't share</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.fsBig} ${privacyStyles.headerGrayBackground}`}>To limit our sharing</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            <ul><li>Call toll-free 1-866-916-7974</li></ul>
            <strong>Please note:</strong><br />
            If you are a new customer, we can begin sharing your information 30 days from the date we sent this notice.  When you are no longer our customer, we continue to share your information as described in this notice.<br />
            However, you can contact us at any time to limit our sharing.
          </div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col20} ${privacyStyles.bold} ${privacyStyles.fsBig} ${privacyStyles.headerGrayBackground} ${privacyStyles.vmiddle}`}>Questions?</div>
          <div className={`${privacyStyles.column} ${privacyStyles.col80} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>Call 1-866-916-7974 or go to <a className={privacyStyles.regularLink} href="https://www.equilife.com" target="_blank" rel="noopener noreferrer">www.equilife.com</a>.</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}>Who we are</div>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}></div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>Who is providing this notice?</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>Equitable Life & Casualty Life Insurance Company and its affiliates (collectively, “Equitable”). <br />See list of affiliates below.</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}>What we do</div>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}></div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>How does Equitable protect my personal information?</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>To protect your personal information from unauthorized access and use, we use security measures that comply with federal law. These measures include computer safeguards and secured files and buildings. To learn more about security at Equitable, please visit <a className={privacyStyles.regularLink} href="https://www.equilife.com" target="_blank" rel="noopener noreferrer">www.equilife.com</a></div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>How does Equitable collect my personal information?</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            We collect your personal information, for example, when you
            <ul className={privacyStyles.mn}>
              <li>Apply for insurance or pay insurance premiums</li>
              <li>File an insurance claim or give us your income information</li>
              <li>Provide employment information</li>
            </ul>
            We also collect your personal information from others, such as affiliates, or other companies. We may also sometimes request your permission to collect personal health information from your doctor, hospitals, or other medical providers.
          </div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>Why can’t I limit all sharing?</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            Federal law gives you the right to limit only
            <ul className={privacyStyles.mn}>	
              <li>Sharing for affiliates’ everyday business purposes - information about your creditworthiness</li>
              <li>Affiliates from using your information to market to you</li>
              <li>Sharing for nonaffiliates to market to you</li>
            </ul>
            State laws and individual companies may give you additional rights to limit sharing. See below for more on your rights under state law.
          </div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>What happens when I limit sharing for an account I hold jointly with someone else?</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>Your choices will apply to everyone on your account – unless you tell us otherwise.</div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}>Definition</div>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}></div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>Affiliates</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            Companies related by common ownership or control. They can be financial and nonfinancial companies.
            <ul className={privacyStyles.mn}>
              <li>Equitable National Life Insurance Company, Inc.</li>
              <li>Sterling Investors Life Insurance Company</li>
            </ul>
          </div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>Nonaffiliates</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            Companies not related by common ownership or control.  They can be financial and nonfinancial companies.
            <ul className={privacyStyles.mn}><li>Equitable does not share with nonaffiliates so they can market to you.</li></ul>
          </div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.col40} ${privacyStyles.fsTiny}`}><strong>Joint marketing</strong></div>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny} ${privacyStyles.vmiddle}`}>
            A formal agreement between nonaffiliated financial companies that together market financial products or services to you.
            <ul className={privacyStyles.mn}><li>Equitable does not jointly market.</li></ul>
          </div>
        </div>
      </div>
      
      <div className={`${privacyStyles.table} ${privacyStyles.mb10}`}>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.bold} ${privacyStyles.headerGrayBackground} ${privacyStyles.bordern}`}>Other important information</div>
        </div>
        <div className={privacyStyles.row}>
          <div className={`${privacyStyles.column} ${privacyStyles.fsTiny}`}>
            <p><strong>For Vermont Residents Only:</strong> We will automatically limit sharing of your information.</p>

            <p><strong>For California Residents Only:</strong> We will not share your information we collect about you with nonaffiliated third parties, except as permitted by California law, such as to process your transactions or to maintain your policy.</p>

            <p><strong>For Nevada Residents Only:</strong> We are providing this notice pursuant to Nevada state law. If you prefer not to receive marketing calls from us, you may be placed on our internal do-not-call list by calling 1-800-352-5170. For more information, you may contact our customer service department or the Nevada Attorney General at 555 E. Washington Ave., Suite 3900, Las Vegas, NV 89101; 702-486-3132; <a className={privacyStyles.regularLink} href="mailto:aginfo@ag.nv.gov">aginfo@ag.nv.gov</a>.</p>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default CCPAPrivacyPolicyPage
