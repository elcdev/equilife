import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import BBannerCenter from '../images/B+ banner-center.png'
import AboutUsMiddle from '../images/aboutusmiddle.png'
import AboutUsImage from '../images/AboutUs.png'

const AboutUsPage = () => (
  <Layout>
    <SEO title='About Us' />
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'> 
          <img className='fill' src={AboutUsImage} alt='Atlas Intro'></img> 
        </div>
      </div>
    </div>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <div className='image'> <img className='image' src={BBannerCenter}
              style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '37%', width: 'auto/9' }} alt='B+ banner' />
            </div>
            <br />
            <h3 className='section-title underline longer-underline'><i className='fas fa-flag-usa'></i> About Us</h3>
            <p className='section-main'>
              <p>Equitable Life & Casualty Insurance Company was founded in 1935 and is Utah’s oldest active
              life insurer. SILAC, Inc. purchased Equitable in April 2017, bringing both access to capital
              and strong, experienced leadership. Equitable is licensed in 46 states and the District of
              Columbia.
                            </p>
              <p>Throughout its 80+ year history, Equitable has been
              a pioneer in emerging markets. In 1965, Medicare Supplement Insurance was developed,
              followed by Equitable beginning to develop and market other supplemental health products in
              1974. This tradition continues with the introduction of Fixed Annuities in 2018 and Fixed
              Index Annuities in 2019.
                            </p>
              <p>Equitable will continue to draw on its long history but also look towards the future,
              developing products for our current and future policyholders in order to help with their
              savings and retirement needs, supplemental health insurance, and the ability to leave a
              financial legacy for their family.
                            </p>
              <p>The character of the Equitable family is what truly sets us apart. Over the years, we’ve
              earned a national reputation for caring. Its value cannot be measured in dollars; and we
              passionately guard and preserve it.
                            </p>
              <p className='section-subtitle'>We know that our policyholders & distribution partners all have
              options; and we are focused on Equitable Life & Casualty being your #1 choice.
                            </p>
              <div className='image'> <img className='image' src={AboutUsMiddle}
                style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '75%', width: 'auto/9' }}
                alt='about us mid' />
              </div>
            </p>
          </div>
        </div>
      </div>
    </section>



  </Layout >
)

export default AboutUsPage
