import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'
import Slideshow from '../components/common/slideshow'

// import AtlasIntro from '../images/AtlasIntro.jpg'
const imageList = ['AtlasIntro.JPG', 'WebBanners4.png', 'WebBanners22.png', 'WebBanners5.png', 'WebBanners3.png', 'WebBanners19.png', 'WebBanners16.png', 'WebBanners24.png', 'WebBanners23.png']

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isVision: true };
  }

  switchMessage(isVision) {
    if (this.state.isVision ^ isVision) {
      var vision = document.getElementById('vision');
      var mission = document.getElementById('mission');
      var visionButton = document.getElementById('visionButton');
      var missionButton = document.getElementById('missionButton');
      if (isVision) {
        visionButton.setAttribute('class', 'active');
        missionButton.removeAttribute('class');
        vision.setAttribute('class', 'display');
        mission.setAttribute('class', 'hide');
      } else {
        missionButton.setAttribute('class', 'active');
        visionButton.removeAttribute('class');
        mission.setAttribute('class', 'display');
        vision.setAttribute('class', 'hide');
      }
      this.setState({ isVision: isVision });
    }

  }

  render() {
    return (
      <Layout>
        <SEO title='Home' />

        
        <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
          <div className='carousel-inner'>
            <div className='item active'>
              <Slideshow slides={imageList} />
            </div>
          </div>
        </div>

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Launch demo modal
        </button>

        {/* <!-- Modal --> */}
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>


        <section className='content-section'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-md-offset-2' style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                <div className='tab-style-5'>
                  <ul className='nav-tabs' style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                    <li id='visionButton' className='active'><button onClick={() => { this.switchMessage(true); }}>
                      <span className='fa fa-eye'></span> Vision</button></li>
                    <li id='missionButton'><button onClick={() => { this.switchMessage(false); }} >
                      <span className='fa fa-map-marked-alt'></span> Mission</button></li>
                  </ul>


                  <div className='tab-content'>
                    <div className='display' id='vision'>
                      <p>Equitable Life & Casualty ('Equitable') focuses exclusively on offering
                      innovative and affordable retirement solutions by providing outstanding service
                          and offering Fixed Annuity & Insurance products that fill an important need.</p>
                      <p>At Equitable Life & Casualty, we specialize in helping Americans prepare for
                          retirement and leave lasting legacies for their loved ones.</p>
                    </div>
                    <div className='hide' id='mission'>
                      <p>Throughout its 80+ year history, Equitable has been a pioneer in emerging
                      markets. In 1965, Medicare Supplement Insurance was developed, followed by
                          Equitable beginning to develop and market Long-Term Care products in 1974.</p>
                      <p>This tradition continues with the introduction of Fixed Annuities in 2018 and
                          Fixed Index Annuities, recently launched this June. </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <section className='content-section'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-12 section-header text-center'>
                <h3 className='section-title underline'>Our Guiding Principles</h3>
                <p className='section-subtitle'>Equitable knows that ongoing success is primarily due to four
                            factors: </p>
              </div>
              <div className='col-md-6'>
                <div className='feature-box'>
                  <div className='icon'>
                    <i className='fa fa-hands-helping'></i>
                  </div>
                  <h5 className='feature-title'><b>1. Dedicated Employees</b></h5>
                  <p className='feature-desc'>Talented and dedicated employees who are committed to providing
                                exceptional service.</p>
                </div>
              </div>
              <div className='col-md-6'>
                <div className='feature-box'>
                  <div className='icon'>
                    <i className='fa fa-network-wired'></i>
                  </div>
                  <h5 className='feature-title'><b>2. Strong Relationships</b></h5>
                  <p className='feature-desc'>Strong relationships with our distribution partners, built on trust
                                and outstanding agent services.</p>
                </div>
              </div>
              <div className='col-md-6'>
                <div className='feature-box'>
                  <div className='icon'>
                    <i className='fa fa-chart-line'></i>
                  </div>
                  <h5 className='feature-title'><b>3. Product Development & Enhancements</b></h5>
                  <p className='feature-desc'>We deliver exceptional customer value matched with disciplined risk
                                management.</p>
                </div>
              </div>
              <div className='col-md-6'>
                <div className='feature-box'>
                  <div className='icon'>
                    <i className='fa fa-heart'></i>
                  </div>
                  <h5 className='feature-title'><b>4. Everyday Excellence</b></h5>
                  <p className='feature-desc'>The guiding principle that everyday excellence is the standard - for
                                our customers, our agents, our business partners, and our employees.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className=''>
          <div className='container'>
            <div className='row'>
              <div className='col-md-12 mt40 mb40'>
                <div className='cta-3'>
                  <h3 className='title'>Talk To Us!</h3>
                  <p className='desc'>Call us because we're awesome and would love to help!</p>
                  <Link to='contact' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Contact Us</Link>
                  <div className='clearfix'></div>
                  <span className='far fa-paper-plane bg-icon icon-rotate'></span>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='content-section'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-3 col-sm-6'>
                <div className='counter-box text-center'>
                  <div className='count-info'>
                    <h6><span className='counter'>80</span>+</h6>
                    <p><b>years in business.</b></p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 col-sm-6'>
                <div className='counter-box text-center'>
                  <div className='count-info'>
                    <h6><span className='counter'>500,000</span>+</h6>
                    <p><b>delighted clients.</b></p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 col-sm-6'>
                <div className='counter-box text-center'>
                  <div className='count-info'>
                    <h6><span className='counter'>7,000</span>+</h6>
                    <p><b>cups of coffee.</b></p>
                  </div>
                </div>
              </div>
              <div className='col-md-3 col-sm-6'>
                <div className='counter-box text-center'>
                  <div className='count-info'>
                    <h6><span className='counter'>180</span>+</h6>
                    <p><b>happy Home Office employees!</b></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </Layout>
    )
  }
}

export default IndexPage
