import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import HeaderImage from '../images/Header_20.png'
import ETeamLogo from '../images/E-Team Rubber Stamp-adj.png'
import POSLogo from '../images/POSmiddle.png'

const POSPage = () => (
  <Layout>
    <SEO title='Policy Service Department' />
    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={HeaderImage} alt='Policy Service Department' />
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title'>Equitable's Policyowner Service Department:</h3>
            <h2><i>now known as</i></h2>
            <h3 className='section-title underline longer-underline'><b>Your Equitable E-Team!</b></h3>
            <div className='image'> <img className='image' src={ETeamLogo}
              style={{margin: '0 auto', maxHeight: '100%', maxWidth: '80%', width:'auto/9'}}
              alt='e-team logo' />
              <p className='section-subtitle'>We care... we listen... and we respond to your needs! </p>
              <p>The Equitable E-Team’s family perspective, traditionally high standards, dedicated
              teamwork, and insistence on being the best has earned Equitable a reputation for being a
              caring leader.

                    The products and personalized services that the E-Team provides are second to none. </p>
              <p>When you call Equitable, you talk to a friendly, helpful person - not a machine. We enjoy
                    getting to know you personally!</p>

              <p>Professional representatives are available Monday through Friday from 7:00 a.m. to
                    5:30 p.m. Mountain Time. </p>

              <p>Whether we are responding to an e-mail, in writing, or on the telephone, we take
              pride in every opportunity we have to serve our customers. We deliver the
                    service that exceeds your expectations! </p>
            </div>
          </div>
        </div>

        <div className='image'> <img className='image' src={POSLogo}
          style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '100%', width: 'auto/9' }} alt='pos logos' />
        </div>
      </div>
    </section>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>

        <div className='col-md-4 feature text-center' style={{float: 'left'}}>
          <div className='icon'>
            <i className='fa fa-file-signature'></i>
          </div>
          <h5 className='feature-title'>Forms</h5>
          <p className='feature-desc'>We make it easy and efficient for our Policyowners to keep us informed of
            their needs. <br />
            Start here to update your Personal Information, Policy Information, request duplicate documents,
            and more.</p>
          <p><Link to='forms' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Common Forms</Link></p>
        </div>
        <div className='col-md-4 feature text-center' style={{float: 'left'}}>
          <div className='icon'>
            <i className='fa fa-search-dollar'></i>
          </div>
          <h5 className='feature-title'>Claims Inquiry</h5>
          <p className='feature-desc'>Wondering about a claim? <br />
            Have a question about a policy?</p>
          <p>Click below to submit your comments<br />
            or questions.</p>
          <p><Link to='contact' className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Contact</Link></p>
        </div>
        <div className='col-md-4 feature text-center' style={{float: 'left'}}>
          <div className='icon'>
            <i className='fa fa-check'></i>
          </div>
          <h5 className='feature-title'>SE HABLA ESPANOL</h5>
          <p className='feature-desc'>Hacemos todo lo posible para ayudar los miembros de la familia de Equitable
          que hablan espanol.
            Si tiene preguntas o necesita ayuda, llame a nuestro:<br />
            <b>1-877-579-3751.</b></p>
          <p>Se le pedira dejar un mensaje, y un representante que hable espanol le llamara lo mas pronto
            posible.</p>
        </div>
		</div>
      </div>
    </section>

  </Layout >
)

export default POSPage
