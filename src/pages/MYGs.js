import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

import MYGImage from '../images/MYGs.png'
import SSEliteImage from '../images/SSElite logo sm.png'
import SSElogoImage from '../images/SSElogo.png'
import SSlogoImage from '../images/SSlogo.png'
import EliteBrochure from '../assets/2019 SS ELITE Brochure.pdf'
import SecureSavingsBrochure from '../assets/Secure Savings Brochure 2019.pdf'

const MYGPage = () => (
  <Layout>
    <SEO title='Multi-Year Guarantee Annuities' />

    <div id='slider' className='carousel slide carousel-style-4' style={{ height: '60vh' }}>
      <div className='carousel-inner'>
        <div className='item active'>
          <img className='fill' src={MYGImage} alt='Multi-Year Gaurentee Annuities'/>
          <div className='carousel-caption'>
          </div>
        </div>
      </div>
    </div>
    <section className='content-section'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-12 section-header text-center'>
            <h3 className='section-title underline longer-underline'><i className='fa fa-hand-holding-usd'></i>
                            Multi-Year Guarantee Annuities</h3>
            <div className='col-md-12 section-header text-center image-center'>
              <p className='section'>Equitable Secure Savings™ and Secure Savings™ Elite will credit
                                policyowners with a fixed interest rate every year. </p>
              <p className='section-subtitle'>That means, no matter what happens with the stock market,
              Equitable Secure Savings™ Series will credit you a fixed interest rate every year – even
                                if the stock market declines.</p>
              <p className='section'>This rate will be guaranteed for the guarantee period you select, either
                                2 or 5 years.</p>
              <div className='image'> <img className='image' src={SSEliteImage}
                style={{ maxHeight: '100%', maxWidth: '50%', width: 'auto/9' }} alt='SSE logos' />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className='content-section'>
      <div className='container'>
        <div className='row'>



          <div className='col-md-6 feature text-center'>
            <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '55%', width: 'auto/9' }}>
              <img className='img-thumbnail' src={SSlogoImage} alt='Secure Saving Logo' />
            </div>
            <p></p>
            <p className='feature-desc'>The <b>Secure Savings</b> is our flagship MYGA series that provides the
                            highest guaranteed fixed interest rates that we offer paired with built-in liquidity
                            provisions.</p>

            <p><b> If you’re looking for the highest guaranteed interest rate possible with built in
                                    liquidity, then Secure Savings is your solution!</b></p>

            <p>It’s a single premium Multi-Year Guaranteed Annuity series with a 2- and 5-year rate
                                guarantee period to choose from. </p>

            <p>Annual free withdrawals, full account value at death and spousal continuation are
                                automatically included free of charge.</p>
            <p><a href={SecureSavingsBrochure}
              className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about Equitable
                                    Secure Savings</a></p>
          </div>

          <div className='col-md-6 feature text-center'>
            <div className='img-thumbnail' style={{ margin: '0 auto', maxHeight: '100%', maxWidth: '65%', width: 'auto/9' }}>
              <img className='img-thumbnail top' src={SSElogoImage} alt='Secure Saving Elite Logo' />
            </div>
            <p></p>
            <p className='feature-desc'>The <b>Secure Savings Elite</b> offers a higher credited rate than the
                            Secure
                            Savings™ and and allows you to customize the product to best fit your unique
                            needs. Cash surrender value is payable at the death of the owner. Depending
                            on the guarantee period chosen, it may offer liquidity options for a lower
                            credited rate.</p>

            <p><b>If you’re looking for the highest guaranteed interest rate possible, then go with the
                                    Secure Savings Elite! </b></p>

            <p>
              Optional free withdrawal benefits are available on the 2-year version for a slightly
              reduced interest rate. Optional free withdrawal benefits are not available on the 5-year
                                version at this time. Spousal continuation at death is automatically included. </p>
            <p><a href={EliteBrochure}
              className='st-btn primary-btn hvr-back hvr-sweep-to-right'>Learn more about Secure
                                    Savings Elite</a></p>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default MYGPage
