import React from 'react'
import Layout from '../components/equilife/layout'
import SEO from '../components/common/seo'

const USAMCOPage = () => (
  <Layout>
    <SEO title='USA Manage Care Organization' />
    <section className='content-section'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-12 section-header text-center'>
            <br />
            <h3 className='section-title underline'>USA Managed Care Organization</h3>
            <p className='section-subtitle'>Discounts on your Medicare Part A Deductible</p>
            <p className='section-main'>As a valued Medicare Supplement policyowner we are pleased to make available
                        a service we offer, free of charge.</p>
            <p>We have an arrangement with a nationwide group of hospitals through USA Managed Care Organization
            (USA MCO). If you require a hospital stay those hospitals that are contracted as part of the USA
            MCO group have agreed to discount all or a part of your Medicare inpatient hospital deductible.
                    </p>
            <p>To identify the hospitals in your area that are part of the USA MCO group please contact USA MCO
            at 800-872-3860. When you are admitted to the hospital be sure to show them your Equitable I.D.
                        card; it has the logo of USA MCO imprinted on it. That's all you need to do.</p>

            <p>Once Medicare processes your claim, it is sent to us electronically. We will handle your claim
            for Medicare Supplement benefits. If there is an inpatient deductible applicable to your stay
                        with a USA MCO participating hospital, the discount will be taken.</p>
            <p>As part of the Equitable Family your continued satisfaction is very important to us. If you need
                        assistance, we are only a phone call away.</p>
            <p>Our toll free phone number is <b>800-352-5160.</b> We will do everything in our power to
                            substantiate the trust and confidence which you have placed in us.</p>

            <p><b>Offering an <a title='List of Participating Hospitals'
              href='http://www.usamco.com/equitable/' target='_blank' rel="noopener noreferrer">online list</a> of
                                participating hospitals in your area.</b></p>

            <p>This is a commercial message from Equitable Life & Casualty Insurance Company, a private
            insurance company which is not connected with or endorsed by the United States Government or
                            the federal Medicare program.</p>
          </div>
        </div>
      </div>
    </section>

  </Layout>
)

export default USAMCOPage
