import { Link } from 'gatsby'
import React from 'react'

import DarkLogo from '../../images/logo-dark.png'
import '../../sass/equilife/_header.sass'


class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false };
  }


  componentDidMount() {
    this.setState({ loaded: true });
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions.bind(this));

  }

  updateDimensions() {
    var drops = document.getElementsByClassName('drop');
    if (document.body.clientWidth < 1141) {
      Array.from(drops).forEach(element => {
        element.style.display = 'none';
      });
    } else {
      Array.from(drops).forEach(element => {
        element.style.display = 'block';
      });
    }
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions.bind(this));
  }

  myFunction() {
    var x = document.getElementsByClassName('drop');
    Array.from(x).forEach(element => {
      if (element.style.display === 'block') {
        element.style.display = 'none';
      } else {
        element.style.display = 'block';
      }
    });

  }

  display(id) {
    var element = document.getElementById(id);
    if (element.getAttribute('class') === 'panel-collapse') {
      element.setAttribute('class', 'panel-collapse collapse');
    } else {
      var elements = document.getElementsByClassName('panel-collapse');
      Array.from(elements).forEach(e => {
        e.setAttribute('class', 'panel-collapse collapse');
      });
      element.setAttribute('class', 'panel-collapse');
    }

  }


  render() {

    if (!this.state.loaded) {
      return (<div className='page-loader'>
        <img src={DarkLogo} alt='ELC' width='260' className='logo-img logo-loader animated infinite pulse' />
      </div>);
    }

    return (
      <header className='header-shadow light-header semi-trans-header'>

        {/* ========== HEADER ========== */}
        <div className='header menu-style-10'>

          {/* ========== Covid Message ========== */}
          <div className='row'>
            <div className='col-md-12 col-sm-12'>
              <div className='alert alert-warning text-center' role='alert'>
                <i className='fas fa-exclamation-triangle'>&nbsp;</i>Please <Link to='covid-19' className='alert-link'>click here</Link> for Equitable Life's response to <wbr />COVID-19<wbr />.
                </div>
            </div>
          </div>

          <div className='container'>
            {/* ========== MENU ========== */}
            <nav className='menu'>
              <div className='panel-group main-menu' id='accordion' role='tablist' aria-multiselectable='true'>
                <div className='panel logo-header'>
                  <div className='panel-heading logo-header' role='tab' id='logo'>
                    <Link to='/'><img src={DarkLogo} alt='ELC' className='logo-img' /></Link>
                    <button className='menu-mobile' onClick={this.myFunction} style={{ marginTop: 'auto', marginBottom: 'auto', marginLeft: 7, cursor: 'pointer', background:'none',border:'none' }}>
                      <div style={{ width: 25, height: 3, backgroundColor: '#000000', margin: '6px 0' }}></div>
                      <div style={{ width: 25, height: 3, backgroundColor: '#000000', margin: '6px 0' }}></div>
                      <div style={{ width: 25, height: 3, backgroundColor: '#000000', margin: '6px 0' }}></div>
                    </button>
                  </div>
                </div>
                <div className='panel drop'>
                  <div className='panel-heading' role='tab' id='headingZero'>
                    <h4 className='panel-title'>
                      <Link to='/' role='button'>Home</Link>
                    </h4>
                  </div>
                </div>
                <div className='panel drop' id='products'>
                  <div className='panel-heading' role='tab' id='headingOne'>
                    <h4 className='panel-title'>
                      <a className='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' style={{ cursor: 'pointer' }}
                        onClick={() => { this.display('collapseOne'); }} aria-expanded='false' aria-controls='collapseOne'>
                        Products
                                </a>
                    </h4>
                  </div>
                  <div id='collapseOne' className='panel-collapse collapse' role='tabpanel'
                    aria-labelledby='headingOne'>
                    <div className='list-group'>
                      <li className='list-group-item'>
                        <Link to='products'><i className='fas fa-shield-alt'></i> Overview</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='FIAs'><i className='fas fa-piggy-bank'></i> Fixed Index Annuities</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='MYGs'><i className='fas fa-hand-holding-usd'></i> Multi-Year Guarantee
                                        Annuities</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='additional'><i className='fas fa-comments-dollar'></i> Additional
                                        Products</Link>
                      </li>
                    </div>
                  </div>
                </div>
                <div className='panel drop'>
                  <div className='panel-heading' role='tab' id='headingTwo'>
                    <h4 className='panel-title'>
                      <a className='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' style={{ cursor: 'pointer' }}
                        onClick={() => { this.display('collapseTwo'); }} aria-expanded='false' aria-controls='collapseTwo'>
                        Policyowner Service
                                </a>
                    </h4>
                  </div>
                  <div id='collapseTwo' className='panel-collapse collapse' role='tabpanel'
                    aria-labelledby='headingTwo'>
                    <div className='list-group'>
                      <li className='list-group-item'>
                        <Link to='POS'><i className='fas fa-heartbeat'></i> The E-Team</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='forms'><i className='fa fa-file-signature'></i> Forms</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='contact'><i className='fa fa-search-dollar'></i> Claims Inquiry</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='espanol'><i className='fa fa-check'></i> Se Habla Espanol</Link>
                      </li>
                    </div>
                  </div>
                </div>
                <div className='panel drop' id='about'>
                  <div className='panel-heading' role='tab' id='headingThree'>
                    <h4 className='panel-title'>
                      <a className='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' style={{ cursor: 'pointer' }}
                        onClick={() => { this.display('collapseThree'); }} aria-expanded='false' aria-controls='collapseThree'>
                        About
                                </a>
                    </h4>
                  </div>
                  <div id='collapseThree' className='panel-collapse collapse' role='tabpanel'
                    aria-labelledby='headingThree'>
                    <div className='list-group'>
                      <li className='list-group-item'>
                        <Link to='aboutus'><i className='fas fa-flag-usa'></i> Who We Are</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='leadership'><i className='fas fa-mountain'></i> Leadership Team</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='stability'><i className='fas fa-chart-pie'></i> Financial Stability</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='working'><i className='fas fa-home'></i> Working Here</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='repELC'><i className='fas fa-users'></i> Representing ELC</Link>
                      </li>
                      <li className='list-group-item'>
                        <Link to='contact'><i className='fa fa-question-circle'></i> Contact Us!</Link>
                      </li>
                    </div>
                  </div>
                </div>
                <div className='panel drop' >
                  <div className='panel-heading' role='tab' id='headingFour'>
                    <h4 className='panel-title'>
                      <Link role='button' to='contact'><i className='fa fa-question-circle' /> Contact</Link>
                    </h4>
                  </div>
                </div>
                <div className='panel drop'>
                  <div className='panel-heading' role='tab' id='headingFive'>
                    <h4 className='panel-title'>
                      <a role='button' href="/CCPA-PrivacyPolicy">
                        <i className='fa fa-external-link-alt'></i> Privacy
                      </a>
                    </h4>
                  </div>
                </div>
                <div className='panel drop'>
                  <div className='panel-heading' role='tab' id='headingSix'>
                    <h4 className='panel-title'>
                      <Link role='button' to='clientLanding'>
                        <i className='fa fa-external-link-alt'></i> Clients
                                </Link>
                    </h4>
                  </div>
                </div>
                <div className='panel drop'>
                  <div className='panel-heading' role='tab' id='headingSeven'>
                    <h4 className='panel-title'>
                      <a role='button' href='https://www.equiline.com'>
                        <i className='fa fa-external-link-alt'></i> Producers
                                </a>
                    </h4>
                  </div>
                </div>

              </div>
            </nav>
          </div>
        </div>
      </header>
    )
  }
}


export default Header
