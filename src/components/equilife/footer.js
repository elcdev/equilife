import React from 'react'
import { Link } from 'gatsby'

import WhiteLogo from '../../images/logo-white.png'
import '../../sass/equilife/_footer.sass'

const Footer = (() =>
  <footer className='footer dark-footer footer-style-3' id='footer'>
    <div className='row' style={{ textAlign: 'left' }}>
      <div className='col-md-8 col-sm-8' style={{ marginRight: 'auto', marginLeft: 'auto', width: '100%' }}>
        <div className='vcenter mb20'>
          <img src={WhiteLogo} alt='Equitable Life & Casualty' className='img-responsive mb20'
            width='3150px' style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto', width: '50%' }} />
          <div className=' clearfix'>
            <p className='footer-desc' style={{ textAlign: 'center' }}>
              A commitment to caring since 1935.
            </p>
          </div>
        </div> 
        <div style={{ display: 'block', marginLeft: '7%', marginRight: '7%' }}>
          <div className='col-md-3 col-sm-6 left'>
            <h5 className='footer-title underline'>Products</h5>
            <ul className='ul'>
              <li><Link to='products' className='trans'>Overview</Link></li>
              <li><Link to='FIAs' className='trans'>Fixed Index Annuities</Link></li>
              <li><Link to='MYGs' className='trans'>Multi-Year Guarantee Annuities</Link></li>
              <li><Link to='additional' className='trans'>Additional Products</Link></li>
              <li><Link to='usaManageCare' className='trans'>USA MCO</Link></li>
            </ul>
          </div> 
          <div className='col-md-3 col-sm-6 left'>
            <h5 className='footer-title underline'>POS</h5>
            <ul className='ul'>
              <li><Link to='POS' className='trans'>The E-Team</Link></li>
              <li><Link to='forms' className='trans'>Forms</Link></li>
              <li><Link to='contact' className='trans'>Claims Inquiry</Link></li>
              <li><Link to='espanol' className='trans'>Se Habla Espanol</Link></li>
            </ul>
          </div> 
          <div className='col-md-3 col-sm-6 left'>
            <h5 className='footer-title underline'>About Us</h5>
            <ul className='ul'>
              <li><Link to='aboutus' className='trans'>Who We Are</Link></li>
              <li><Link to='leadership' className='trans'>Leadership Team</Link></li>
              <li><Link to='stability' className='trans'>Financial Stability</Link></li>
              <li><Link to='working' className='trans'>Working Here</Link></li>
              <li><Link to='repELC' className='trans'>Representing Equitable</Link></li>
              <li><Link to='contact' className='trans'>Contact Us</Link></li>
            </ul>
          </div> 
          <div className='col-md-3 col-sm-6 left'>
            <h5 className='footer-title underline'>Call Us Toll Free</h5>
            <h2>+1 (800) 352-5150</h2>
            <h6 className='lowercase mt5 mb20'><a
              href='mailto:service@equilife.com'><u>Service@Equilife.com</u></a></h6>
            <p className='footer-desc address'>
              <i className='icon fa fa-map-marker'></i><br />
                        299 South Main Street<br />
                        Suite 1100<br />
                        Salt Lake City, Utah 84111<br />
            </p> 
          </div> 
        </div> 
      </div>
    </div> 
    <div className='container footer-middle'>
      <div className='row'>
        <div className='col-md-6'>
          <h6 className='footer-title lowercase'><i className='fa fa-phone'></i> +1 (800) 352-5150 <span
            className='ml10 mr10'>|</span> <i className='fa fa-envelope'></i> service@equilife.com</h6>
        </div>
        <div className='col-md-6 text-right'>
          <a href='http://www.linkedin.com/company/equitablelife' className='trans pr10 pl10'><i
            className='fab fa-linkedin'></i></a>
          <a href='http://www.facebook.com/equitabledirect' className='trans pr10 pl10'><i
            className='fab fa-facebook'></i></a>
        </div> 
      </div> 
    </div> 
    <div className='footer-last'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-6'>
            &copy; 2019 Equitable Life & Casualty
            </div>
          <div className='col-md-6 text-right'>
            <Link to='termsofuse' className='mr10'>Terms of Use</Link> <a
              href="/CCPA-PrivacyPolicy">CCPA Privacy Policy</a>
          </div>
        </div> 
      </div> 
    </div>

    {/* ========== Scroll to top button ========== */}
    <div className='scroll-to-top trans'><i className='fa fa-angle-up'></i></div>
  </footer>
)


export default Footer


