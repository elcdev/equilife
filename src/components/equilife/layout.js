import React from 'react'
import PropTypes from 'prop-types'

import Header from './header'
import Footer from './footer'

import 'bootstrap/dist/css/bootstrap.min.css';
import '../../sass/equilife/main.css';

const Layout = ({ children }) => {

  return (
    <>
      <link href='https://fonts.googleapis.com/css?family=Lato&display=swap' rel='stylesheet'></link>
      <script src='https://kit.fontawesome.com/33b05a1335.js'></script>
      <Header label='Home' />
      <main>
        <div className='main covid' style={{ paddingTop: 40 }}>
          {children}
        </div> 
      </main>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
