import React from 'react'
import PropTypes from 'prop-types'
import BackgroundSlider from 'gatsby-image-background-slider'
import { useStaticQuery, graphql } from 'gatsby'

const Slideshow = ({slides}) => (
  <BackgroundSlider
    query={useStaticQuery(graphql`
        query {
          backgrounds: allFile (filter: {sourceInstanceName: {eq: "images"}}){
            nodes {
              relativePath
              childImageSharp {
                fluid (maxWidth: 4000, quality: 100){
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      `)}
    initDelay={8} // delay before the first transition (if left at 0, the first image will be skipped initially)
    transition={4} // transition duration between images
    duration={8} // how long an image is shown
    images={slides}//['AtlasIntro.JPG', 'WebBanners4.png', 'WebBanners22.png', 'WebBanners5.png', 'WebBanners3.png', 'WebBanners19.png', 'WebBanners16.png', 'WebBanners24.png', 'WebBanners23.png']

  />
);

Slideshow.propTypes = {
  slides: PropTypes.node.isRequired,
}

export default Slideshow